# UNDER CONSTRUCTION

The python file master_travel.py has the entire implementation of the method, and the lifelong.ipynb and vanilla.ipynb notebooks present how to use the models and run the experiments.

However, it is poorly coded and commented, thus, the author is rewriting the code in a more "_pythonic_" way to make it understandable by other researchers.

The main milestones of this project includes:

- Finishing the rewrite for the gitlab.

- Add the references to papers resulted from this research.

- Later, make it available in the Hugging Faces repository.

- Improve the methods, with more research and experiments.
