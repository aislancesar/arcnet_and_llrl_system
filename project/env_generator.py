from numpy import (
    argwhere,
    array,
    cos,
    exp,
    integer,
    issubdtype,
    ones_like,
    pi,
    sin,
    sqrt,
    zeros)
from numpy.random import choice, permutation, randint, seed
from pandas import DataFrame
from typing import List, Optional, Tuple


def _get_dir(
    root: Tuple[float, float],
    dir: int
    ) -> Tuple[float, float]:
    '''
    Given a starting position and a direction, this method
    computes the next position on a hexagonal grid.

    args:
        root: Tuple[float, float]
        - The position of the origin in a 2D grid, as (x, y)

        dir: int
        - The direction of the movement, such that:
            0 - up, +y
            1 - up right, +x +y
            2 - down right, +x -y
            3 - down, -y
            4 - down left, -x -y
            5 - up right, -x +y
    '''

    assert issubdtype(type(dir), integer), ("The type is wrong for "
        f"dir. Given type is {type(dir)}, not int.")

    assert dir < 6 and dir >= 0, ("The direction is wrong."
        f"Try to use up 0 to 5 included, not {dir}")

    diff = [
        (0, 1),
        (cos(pi / 6), sin(pi / 6)),
        (cos(pi / 6), -sin(pi / 6)),
        (0, -1),
        (-cos(pi / 6), -sin(pi / 6)),
        (-cos(pi / 6), sin(pi / 6))][dir]
    return tuple(x + y for x, y in zip(root, diff))


def _comp_dist(
    A: Tuple[float, float],
    B: Tuple[float, float]
    ) -> Tuple[float, int]:
    '''
    Computes the distance (euclidean) and the direction
    (hexagonal) of the pair of points A and B.

    args:
        A/B: Tuple[float, float]
        - The positions of the points.

    returns:
        The distance and the direction, where:
            0 - up, +y
            1 - up right, +x +y
            2 - down right, +x -y
            3 - down, -y
            4 - down left, -x -y
            5 - up right, -x +y

    Notice that the direction is in relation to A -> B.
    '''

    x = B[0] - A[0]
    y = B[1] - A[1]
    d = sqrt(x * x + y * y)

    if x > 1e-3 and y > 1e-3:
        r = 1
    elif x > 1e-3 and y < -1e-3:
        r = 2
    elif x < -1e-3 and y > 1e-3:
        r = 5
    elif x < -1e-3 and y < -1e-3:
        r = 4
    elif y > 1e-3:
        r = 0
    else:
        r = 3
    return d, r


def _update_conn(
    graph: List[Tuple[float, float]]
    ) -> List[List[int]]:
    '''
    Given a list of positions in the hexagonal grid, this method
    computes all connections between them, where a connection is
    considered to be between two nodes with 1 of distance.

    args:
        graph: List[Tuple[float, float]]
        - A list of nodes positions.

    returns
        A N sized list, where each position has 6 positions,
        representing a direction, and in each direction the
        connected node.
    '''

    conn = list()
    for i, node_i in enumerate(graph):
        aux = [None] * 6
        for j, node_j in enumerate(graph):
            if i != j:
                d, r = _comp_dist(node_i, node_j)
                if abs(d - 1) < 1e-3:
                    aux[r] = j
        conn.append(aux)
    return conn


def _DFS(
    orig_conn: List[Tuple[int, int]],
    z: int = 0,
    c: int = 0,
    x: Optional[List[int]] = None,
    out_conn: Optional[List[int]] = list()
    ) -> List[int]:
    '''
    Executes a Depth First Search in order to balance the quantity
    of valid actions of the graph.

    args:
        orig_conn: List[Tuple[int, int]]
        - A list with the original set of connections.

        z: int = 0
        - A counter for visited connections.

        c: int = 0
        - A counter for the chosen action.

        x: Optional[List[int]] = None
        - Is a random sequence of actions.

        out_conn: Optional[List[int]] = list()
        - The output list of actions for each connection.
    '''

    if x is None:
        x = permutation(6).tolist()

    if z == len(orig_conn):
        return out_conn

    for i in range(6):
        d = (c + i) % 6
        aux = out_conn + [x[d]]
        new_conn = _build_conn(orig_conn, aux)

        if _test_node(new_conn):
            var = zeros(6)
            for j in out_conn:
                var[j] -= 1

            p = exp(var) / exp(var).sum()
            d = choice(6, p = p)
            aux = _DFS(orig_conn, z+1, d, x, aux)

            if aux is not None:
                return aux
    return None


def _build_conn(
    orig_conn: List[Tuple[int, int]],
    vec: List[int]
    ) -> List[List[Tuple[int, int]]]:
    '''
    Separates the connections in buckets of actions, later it is used
    to solve conflicts between among actions.

    args:
        orig_conn: List[Tuple[int, int]]
        - The original connections graph.

        vec: List[int]
        - A list of already determined connections.

    returns:
        A list of connections for each action.
    '''

    out_conn = [[], [], [], [], [], []]
    for i, j in enumerate(vec):
        out_conn[j].append(orig_conn[i])
    return out_conn


def _test_node(
    new_conn: List[List[Tuple[int, int]]]
    ) -> bool:
    '''
    Tests for conflicts among actions.
    It is done by, for each action, unraveling the ids of all
    positions connected by the given action.
    The list of positions may contain repeated positions, but the set
    cannot, thus if their sizes are different, that means a same
    action may result in two different final positions.

    args:
        new_conn: List[List[Tuple[int, int]]]
        - The lists of connections for each action.

    returns:
        True if there are no conflicts and False otherwise.
    '''

    for x in new_conn:
        l = [n for a in x for n in a]
        s = set(l)
        if len(s) < len(l):
            return False
    return True


def run(
    size: int,
    seed_id: Optional[int] = None,
    weights: Optional[List[int]] = None,
    probs: Optional[List[float]] = None
    ) -> Tuple[List[Tuple[float, float]], DataFrame]:
    '''
    Executes the process of creating the entire environment
    procedurally.

    args:
        size: int
        - The quantity of positions on the graph.

        seed_id: Optional[int] = None
        - Inputs a seed that controls the random process, in
          order to always generate the same env.

        weights: Optional[List[int]] = None
        - Contains the set of possible rewards received for
          executing an action.

        probs: Optional[List[float]] = None
        - Contains the probability of each weight been selected.
          Notice that they have to sum to 1.

    returns:
        An environment represented by the positions of each node,
        for drawing purposes and a DataFrame with the connections
        represented by both nodes ids (s1, s2), a reward column
        (rwd) and the action responsible for changing the
        position (act).
        Notice that the connections are not directed, thus the
        action will result in changing the agent from position s1
        to s2 and the same action takes the agent from s2 to s1.
    '''

    if weights is None:
        weights = [1]
        probs = [1]
    if probs is None:
        probs = ones_like(weights) / len(weights)
        probs = probs.tolist()

    assert len(weights) == len(probs), (
        'weights and probs has to have the same size')
    assert sum(probs) == 1, 'probs has to sum 1'

    if seed_id is not None:
        seed(seed_id)

    # Create the graph's body
    graph: List[Tuple[float]] = [(0, 0)]

    # In order to keep compatibility with previous experiments,
    # this part was kept.
    rnd = choice(6)
    graph += [_get_dir(graph[0], rnd)]
    conn = _update_conn(graph)

    rnd = choice(argwhere(
        array(conn[0]) == None).flatten())
    graph += [_get_dir(graph[0], rnd)]
    conn = _update_conn(graph)
    # End of the compatibility block, it can be erased for new
    # experiments.

    for _ in range(size - len(graph)):
        asize = len(graph)
        while True:
            i = randint(asize)
            conn = _update_conn(graph)
            z = array(conn[i]) == None
            if z.sum() == 0:
                continue

            rnd = choice(argwhere(z).flatten())
            graph.append(_get_dir(graph[i], rnd))
            conn = _update_conn(graph)
            c1 = (array(conn[-1]) == None).sum() <= 4
            c2 = len(graph) < 3
            if c1 or c2:
                break
            del graph[-1]

    # Converts the generated connections to a DataFrame
    conn = _update_conn(graph)
    conn_new = list()

    for i, x in enumerate(conn):
        for k, j in enumerate(x):
            if j is not None and i < j:
                conn_new.append((i, j, k))

    df_conn_new = DataFrame(
        conn_new,
        columns = ['s1', 's2', 'C'])

    # Distributes the received rewards over the connections
    df_conn_new['rwd'] = choice(
        weights,
        size = len(df_conn_new),
        p = probs)

    # Balances the quantity of valid actions, in order to make
    # each action equally valid on the graph
    orig_conn = df_conn_new[['s1', 's2']].values.tolist()
    res = _DFS(orig_conn)
    df_conn_new['act'] = res
    return graph, df_conn_new.drop(columns='C')
