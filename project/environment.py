from env_generator import run as generate
from numpy import array, zeros
from numpy.random import choice, randint, seed
from matplotlib.pyplot import figure, plot, scatter, show, xticks, yticks
from time import time
from typing import Tuple, List


class Environment:
    def __init__(self, level: int):
        '''
        The environment is created on instancing, through the
        function run from the module env_generator.

        args:
            level: int
            - Selects the amount of targets on the environment.
              Notice that the level should be at least 1.
        '''

        assert level > 0, 'level > 0'

        self.action_space = 7  # 6 directions + 1 stamp action
        self.world_size = 20
        self.observation_space = 3 * self.world_size
        # 1. Position of the agent
        # 2. Targets
        # 3. Stamps collected
        p = array([10, 6, 3, 1])

        self.graph, self.conns = generate(
            size = self.world_size,
            seed_id = 71,
            weights = p[::-1],
            probs = p / p.sum())

        self.level = level
        self.reset()
        seed(int(time()))
        self.pos = randint(self.world_size)
        # Counts how many times the task was completed
        self.endings = 0


    def reset(self):
        '''
        Resets the environment, erasing the stamped targets and
        choosing a new set of targets.
        '''
        self.target = choice(self.world_size, self.level, False)
        self.stamps = list()


    def act(self, act: int) -> Tuple[int, int]:
        '''
        Changes the position of the agent or stamp the current
        position.

        args:
            act: int
            - The action to be taken. Where:
                0 - Stamp action
                1 - Action A
                2 - Action B
                3 - Action C
                4 - Action D
                5 - Action E
                6 - Action F

        returns:
            A tuple, where the first item is the reward and the
            second is a 0/1 integer that represents an end to the
            episode.
        '''

        if act:
            # Finds the transition on the DataFrame
            aux_bool = self.conns.s1 == self.pos
            aux_bool |= self.conns.s2 == self.pos
            aux_bool &= self.conns.act == act - 1

            aux_df = self.conns[aux_bool]
            if len(aux_df):
                if aux_df['s1'].squeeze() == self.pos:
                    self.pos = aux_df['s2'].squeeze()
                else:
                    self.pos = aux_df['s1'].squeeze()
                rwd = -aux_df['rwd'].squeeze()
            else:
                # No transition found, thus the action is invalid.
                rwd = -10
            finish = 0

        else:
            finish = 0

            if self.pos in self.target and self.pos not in self.stamps:
                self.stamps.append(self.pos)
                rwd = 0
            else:
                rwd = -10

            if len(self.stamps) == len(self.target):
                rwd = 10
                finish = 1
                self.endings += 1

        return rwd, finish


    def obs(self) -> List[int]:
        '''
        Used to obtain the observation of the current state of the
        environment.

        returns:
            An observation of size self.observation_space.
        '''

        obs = zeros(3 * self.world_size)
        obs[self.pos] = 1
        obs[self.target + self.world_size] = 1
        if len(self.stamps):
            obs[array(self.stamps) + 2 * self.world_size] = 1
        return obs.tolist()


    def show(self):
        '''
        Creates an image that represents the current state of the
        environment.

        The lines represents the set of connections between nodes of
        the graph. Each color is an action, and the line format
        represents the negative reward received by the agent. Where:
            RED - Action A
            YELLOW - Action B
            GREEN - Action C
            CYAN - Action D
            BLUE - Action E
            MAGENTA - Action F

            Dotted lines gives -1 reward.
            Dash-dot lines gives -3 reward.
            Dashed lines gives -6 reward.
            Normal lines gives -10 reward.

        The circles represents the sets positions the agent
        assumes on the environment, the targets and the stamps.
        Where:
            RED - the agent is in the given position.
            GREEN - is a target position.
            CYAN - is a stamped position.
            YELLOW - the agent is over a target position.
            WHITE - the agent is over a stamped position.
        '''

        x = [aux[0] for aux in self.graph]
        y = [aux[1] for aux in self.graph]
        colors = ['r', 'y', 'g', 'c', 'b', 'm']
        ls = {1: ':', 3: '-.', 6: '--', 10: '-'}

        figure(figsize=(6, 8))

        for r, (A, B, D, E) in self.conns.iterrows():
            plot(
                [x[A], x[B]],
                [y[A], y[B]],
                ls=ls[D],
                c=colors[E],
                linewidth=5,
                zorder=0)

        c = zeros((20, 3))
        c[self.pos, 0] = 1
        c[self.target, 1] = 1
        c[self.stamps, 2] = 1
        scatter(x, y, s=300, c=c, edgecolors='k', zorder=2)
        xticks([])
        yticks([])
        show()
