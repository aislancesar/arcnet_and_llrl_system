from time import time
from typing import Callable, Dict, Optional, Set


class inlinePrint:
    def __init__(self, update_rate: int = 1):
        '''
        This class implements a helper for printing information
        without creating new lines.

        args:
            update_rate: int = 1
            - represents the minimal amount of seconds between
              two prints to be shown on screen, any in between
              print statements are ignored.
        '''

        self.newline_last: bool = True
        self.update_rate: int = update_rate
        self.timestamp: int = time()
        self.buffer: str = ""
        self.prev_buffer_size: int = 0

    
    def set_template(
        self,
        template: Callable[[Dict[str, any]], str],
        dependency: Optional[Set[str]] = None
        ) -> str:

        '''
        This method stores a template to help the printing process.
        The template receives a dictionary that will be printed,
        later use .use_template(Dict...) with a dictionary.

        Notice that it does not supports the newlines inside the
        print string.

        args:
            template: Callable[[Dict[str, any]], str]

            dependency: Optional[Set[str]] = None
            - the variables it should have, raises an error if any
              is missing.

        Example:
            # Given a dict = {"A": 1, "B": 2}

            template = lambda dict:
                f"A = {dict['A']} | B = {dict['B']}"
        '''

        self.template = template

        if not dependency:
            vars = dict()

            while True:
                try:
                    template(vars)
                    break
                except KeyError as e:
                    vars[e.args[0]] = "X"

            dependency = vars.keys()

        self.template_dependency: Set[str] = dependency


    def use_template(self, *args, **kwargs) -> None:
        '''
        Just prints using the preset template.

        It may receive a dictionary of a set of kwargs.

        args:
            *args:
            - is the dictionary.

            kwargs:
            - anything goes, given it is in the template.
              Notice that you should not have a keyword called dict.

        Notice that the argument newline specifies if there will be a
        new line after the print statement, the default is False.
        '''

        assert len(args) < 2, f"Too many arguments: {len(args)}"

        dct = args and args[0] or kwargs

        assert isinstance(dct, dict), (
            "You should give a dictionary to the function, "
            f"not a {type(dct)}.")

        not_found = [
            missing for missing in self.template_dependency
            if missing not in dct.keys()]

        assert len(not_found) == 0, f'Missing variable: {not_found}.'

        txt = self.template(dct)

        newline = False
        if 'newline' in dct.keys():
            newline = dct['newline']

        self.print(txt, newline)


    def print(self, txt: str, newline: bool = True) -> None:
        '''
        Implements the general method for printing.

        args:
            txt: str
            - The string to be printed.

            newline: bool = True
            - Adds a new line at the end of the print statement.
        '''

        txt = str(txt)

        assert '\n' not in txt, ("txt should have no newlines."
            f"\nFor reference the string was:\n{txt}")

        if newline and len(self.buffer) > 0:
            print(self.buffer)
            self.buffer = ""
            self.prev_buffer_size = 0
            self.newline_last = True

        if not newline:
            self.buffer = txt
            if time() - self.timestamp > self.update_rate:
                self.timestamp = time()

                aux_buffer_size = len(txt)
                if len(txt) < self.prev_buffer_size:
                    txt += ' ' * (
                        self.prev_buffer_size - aux_buffer_size)

                print(txt, end='\r')

                self.newline_last = False
                self.prev_buffer_size = aux_buffer_size
                self.buffer = ""

        elif not self.newline_last:
            print('\n' + txt)
            self.newline_last = True

        else:
            print(txt)
            self.newline_last = True
