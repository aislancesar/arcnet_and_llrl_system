import tensorflow as tf
import numpy as np
import pandas as pd
import pickle
from matplotlib import pyplot as plt
from IPython import display as ipd
import time
import typing as tp


global_folder = '/content/drive/MyDrive/experiment_outputs'
code_folder = '/content/drive/MyDrive/Colab Notebooks'


class generator:
    def _get_dir(
        self,
        root: tp.Tuple[float, float],
        dir: int
        ) -> tp.Tuple[float, float]:

        diff = [
            (0, 1),
            (np.cos(np.pi / 6), np.sin(np.pi / 6)),
            (np.cos(np.pi / 6), -np.sin(np.pi / 6)),
            (0, -1),
            (-np.cos(np.pi / 6), -np.sin(np.pi / 6)),
            (-np.cos(np.pi / 6), np.sin(np.pi / 6))][dir]
        return tuple(x + y for x, y in zip(root, diff))


    def _comp_dist(
        self,
        A: tp.Tuple[float, float],
        B: tp.Tuple[float, float]
        ) -> tp.Tuple[float, int]:

        x = B[0] - A[0]
        y = B[1] - A[1]
        d = np.sqrt(x * x + y * y)

        if x > 1e-3 and y > 1e-3:
            r = 1
        elif x > 1e-3 and y < -1e-3:
            r = 2
        elif x < -1e-3 and y > 1e-3:
            r = 5
        elif x < -1e-3 and y < -1e-3:
            r = 4
        elif y > 1e-3:
            r = 0
        else:
            r = 3
        return d, r


    def _update_conn(
        self,
        graph: tp.List[tp.Tuple[float, float]]
        ) -> tp.List[int]:

        conn = list()
        for i in range(len(graph)):
            aux = [None] * 6
            for j in range(len(graph)):
                if i != j:
                    d, r = self._comp_dist(graph[i], graph[j])
                    if np.abs(d - 1) < 1e-3:
                        aux[r] = j
            conn += [aux]
        return conn


    def _DFS(
        self,
        orig_conn: tp.List[tp.Tuple[int, int]],
        z: int = 0,
        c: int = 0,
        x: tp.Optional[tp.List[int]] = None,
        out_conn: tp.Optional[tp.List[int]] = list()
        ) -> tp.List[int]:

        if x is None:
            x = np.random.permutation(6).tolist()

        if z == len(orig_conn):
            return out_conn

        for i in range(6):
            d = (c + i) % 6
            aux = out_conn + [x[d]]
            new_conn = self._build_conn(orig_conn, aux)

            if self._test_node(new_conn):
                var = np.zeros(6)
                for j in out_conn:
                    var[j] -= 1

                p = np.exp(var) / np.exp(var).sum()
                d = np.random.choice(6, p = p)
                aux = self._DFS(orig_conn, z+1, d, x, aux)

                if aux is not None:
                    return aux
        return None


    def _build_conn(
        self,
        orig_conn: tp.List[tp.Tuple[int, int]],
        vec: tp.List[int]
        ) -> tp.List[tp.List[int]]:

        out_conn = [[], [], [], [], [], []]
        for i, j in enumerate(vec):
            out_conn[j] += [orig_conn[i]]
        return out_conn


    def _test_node(
        self,
        new_conn: tp.List[tp.List[int]]
        ) -> bool:

        for x in new_conn:
            l = [n for a in x for n in a]
            s = set(l)
            if len(s) < len(l):
                return False
        return True


    def _run(
        self,
        size: int,
        seed: tp.Optional[int] = None,
        weights: tp.Optional[tp.List[int]] = None,
        probs: tp.Optional[tp.List[float]] = None
        ) -> tp.Tuple[tp.List[tp.Tuple[float, float]], pd.DataFrame]:

        if weights is None:
            weights = [1]
            probs = [1]
        if probs is None:
            probs = np.ones_like(weights).tolist()

        assert len(weights) == len(probs), 'weights and probs has to have the same size'
        assert np.sum(probs) == 1, 'probs has to sum 1'

        if seed is not None:
            np.random.seed(seed)

        graph = [(0, 0)]
        rnd = np.random.choice(6)
        graph += [self._get_dir(graph[0], rnd)]
        conn = self._update_conn(graph)

        rnd = np.random.choice(
            np.argwhere(
                np.array(conn[0]) == None).flatten())
        graph += [self._get_dir(graph[0], rnd)]
        conn = self._update_conn(graph)

        for _ in range(size - 3):
            asize = len(graph)
            while True:
                i = np.random.randint(asize)
                conn = self._update_conn(graph)
                z = np.array(conn[i]) == None
                if z.sum() == 0:
                    continue

                rnd = np.random.choice(
                    np.argwhere(z).flatten())
                graph += [self._get_dir(graph[i], rnd)]
                conn = self._update_conn(graph)
                if (np.array(conn[-1]) == None).sum() <= 4:
                    break
                del graph[-1]

        conn = self._update_conn(graph)
        conn_new = list()

        for i, x in enumerate(conn):
            for k, j in enumerate(x):
                if j is not None and i < j:
                    conn_new += [(i, j, k)]

        df_conn_new = pd.DataFrame(
            conn_new,
            columns = ['s1', 's2', 'C'])

        df_conn_new['rwd'] = np.random.choice(
            weights,
            size = len(df_conn_new),
            p = probs)

        orig_conn = df_conn_new[['s1', 's2']].values.tolist()
        res = self._DFS(orig_conn)
        df_conn_new['act'] = res
        return graph, df_conn_new.drop(columns='C')

    @staticmethod
    def run(
        size: int,
        seed: tp.Optional[int] = None,
        weights: tp.Optional[tp.List[int]] = None,
        probs: tp.Optional[tp.List[float]] = None
        ) -> tp.Tuple[tp.List[tp.Tuple[float, float]], pd.DataFrame]:

        return generator()._run(size, seed, weights, probs)


class Environment:
    def __init__(self, level: int):
        assert level > 0, 'level > 0'
        self.action_space = 7  # 6 directions + 1 stamp action
        self.world_size = 20
        self.observation_space = 3 * self.world_size
        # 1. Position of the agent
        # 2. Targets
        # 3. Stamps collected
        p = np.array([10, 6, 3, 1])

        self.graph, self.conns = generator.run(
            size = self.world_size,
            seed = 71,
            weights = p[::-1],
            probs = p / p.sum())

        self.level = level
        self.reset()
        np.random.seed(int(time.time()))
        self.pos = np.random.randint(self.world_size)
        self.endings = 0


    def reset(self):
        self.target = np.random.permutation(self.world_size)[:self.level]
        self.stamps = list()


    def act(self, act: int) -> tp.Tuple[int, int]:
        if act:
            aux_bool = self.conns.s1 == self.pos
            aux_bool |= self.conns.s2 == self.pos
            aux_bool &= self.conns.act == act - 1

            aux_df = self.conns[aux_bool]
            if len(aux_df):
                if aux_df['s1'].squeeze() == self.pos:
                    self.pos = aux_df['s2'].squeeze()
                else:
                    self.pos = aux_df['s1'].squeeze()
                rwd = -aux_df['rwd'].squeeze()
            else:
                rwd = -10
            finish = 0

        else:
            finish = 0

            if self.pos in self.target and self.pos not in self.stamps:
                self.stamps += [self.pos]
                rwd = 0
            else:
                rwd = -10

            if len(self.stamps) == len(self.target):
                rwd = 10
                finish = 1
                self.endings += 1

        return rwd, finish


    def obs(self) -> tp.List[int]:
        obs = np.zeros(3 * self.world_size)
        obs[self.pos] = 1
        obs[self.target + self.world_size] = 1
        if len(self.stamps):
            obs[np.array(self.stamps) + 2 * self.world_size] = 1
        return obs.tolist()


    def show(self):
        x = [aux[0] for aux in self.graph]
        y = [aux[1] for aux in self.graph]
        colors = ['r', 'y', 'g', 'c', 'b', 'm']
        ls = {1: ':', 3: '-.', 6: '--', 10: '-'}

        plt.figure(figsize=(6, 8))

        for r, (A, B, D, E) in self.conns.iterrows():
            plt.plot(
                [x[A], x[B]],
                [y[A], y[B]],
                ls=ls[D],
                c=colors[E],
                linewidth=5,
                zorder=0)

        c = np.zeros((20, 3))
        c[self.pos, 0] = 1
        c[self.target, 1] = 1
        c[self.stamps, 2] = 1
        plt.scatter(x, y, s=300, c=c, edgecolors='k', zorder=2)
        plt.xticks([])
        plt.yticks([])
        plt.show()


class inlinePrint:
    def __init__(self, update_rate: int = 1):
        self.newline_last: bool = True
        self.update_rate: int = update_rate
        self.timestamp: int = time.time()
        self.buffer: str = ""

    
    def set_func(self, func: tp.Callable[[tp.Dict[str, any]], str]) -> str:
        self.func = func
    
    
    def use_func(self, **kwargs) -> None:
        txt = self.func(**kwargs)
        if 'newline' in kwargs.keys():
            newline = kwargs['newline']
        else:
            newline = False
        self.print(txt, newline)


    def print(self, txt: str, newline: bool = True) -> None:

        if newline and len(self.buffer) > 0:
            print(end='\r')
            print(self.buffer)
            self.buffer = ""
            self.newline_last = True

        if not newline:
            self.buffer = txt
            if time.time() - self.timestamp > self.update_rate:
                self.timestamp = time.time()

                print(end='\r')
                print(txt, end='')
                self.newline_last = False
                self.buffer = ""

        elif not self.newline_last:
            print('\n' + txt)
            self.newline_last = True

        else:
            print(txt)
            self.newline_last = True

    
    def __call__(self, txt: str, newline: bool = True) -> None:
        self.print(txt, newline)


aprint = inlinePrint()
tp_afunc = tp.Callable[[tf.Tensor], tf.Tensor]
tp_loss = tp.Callable[[tf.Tensor, tf.Tensor], tf.Tensor]


class RottedModelException(Exception):
    pass


class ARCNet:
    def __init__(
        self,
        inputs: int,
        outputs: int,
        hidden_layers: tp.List[int],
        activation_functions: list,
        dataset_size: int,
        tf_dtype: tp.Optional[tf.DType] = None,
        ) -> None:
        '''
            Defines the model
        '''

        if tf_dtype is None:
            self.tf_dtype = tf.float64
        else:
            self.tf_dtype = tf_dtype

        self.weights: tp.List[int] = list()
        self.activations: tp.List[tp_afunc] = list()
        self.model_size: int = len(hidden_layers) + 1
        self.input_size: int = inputs
        self.output_size: int = outputs
        
        self.create_weight_matrix(inputs, outputs, hidden_layers)
        self.set_activations(activation_functions)

        self.obs_set = None
        self.nxt_set = None
        self.act_set = None
        self.rwd_set = None
        self.end_set = None

        self.aux_obs = list()
        self.aux_nxt = list()
        self.aux_act = list()
        self.aux_rwd = list()
        self.aux_end = list()

        self.get_best = True

        self.update_np_weights()
        self.opt = None
        self.dataset_size = dataset_size
        self.train_setup = False


    def create_weight_matrix(
        self,
        inputs: int,
        outputs: int,
        hidden_layers: tp.List[int]
        ) -> None:
        '''
            Initializes the model given its topology.
        '''

        inp = inputs
        for h in hidden_layers:
            self.new_layer(inp, h)
            inp = h
        self.new_layer(inp, outputs)


    def new_layer(
        self,
        inp: int,
        out: int
        ) -> None:
        '''
            Creates a new layer.
        '''
        aux = tf.initializers.Orthogonal()
        auxer = tf.Variable(aux(shape=(inp, out), dtype=self.tf_dtype))
        auxest = tf.Variable(aux(shape=(1, out), dtype=self.tf_dtype))
        self.weights += [auxer, auxest]


    def set_activations(
        self,
        act_funcs: tp.Union[str, tp.List[str], tp.List[tp_afunc]]
        ) -> None:
        '''
            Sets the activation functions.
        '''

        if type(act_funcs) == list:
            for act in act_funcs:
                self.set_activation(act)
        else:
            for _ in range(self.model_size):
                self.set_activation(act_funcs)


    def set_activation(
        self,
        act: tp.Union[str, tp_afunc]
        ) -> None:
        '''
            Sets an activation function.
        '''

        if type(act) == str:
            act = tf.keras.layers.Activation(act, dtype=self.tf_dtype)
        self.activations += [act]


    def forward(
        self,
        X: tf.Tensor
        ) -> tf.Tensor:
        '''
            Runs a prediction step.
        '''

        y = X

        for i, act in enumerate(self.activations):
            w = self.weights[2 * i]
            b = self.weights[2 * i + 1]
            y = tf.matmul(y, w) + b

            y = act(y)

        return y


    def predict(
        self,
        X: tf.Tensor
        ) -> tf.Tensor:

        return self.forward(X)

        # y = X

        # for i in range(self.model_size - 1):
        #     w, b = self.np_weights[2*i:2*i+2]
        #     y = np.tensordot(y, w, (1, 0)) + b
        #     y = np.maximum(0, y)

        # w, b = self.np_weights[-2:]
        # y = np.tensordot(y, w, (1, 0)) + b

        return y


    def predict_distilation(self, X: tf.Tensor) -> tf.Tensor:
        y = self.forward(X)
        return tf.keras.activations.softmax(y)


    def training_setup(
        self,
        loss_function: tp_loss,
        metric_function: tp.Optional[tp_loss] = None,
        gamma: float = None
        ) -> None:
        '''
            Run this before trainning.
        '''

        self.train_setup = True

        self.reward_decay_factor = gamma

        self.loss_function = loss_function
        self.metric_function = metric_function


    def function(
        self,
        yreal: tf.Tensor,
        ypred: tp.Optional[tf.Tensor] = None,
        x: tp.Optional[tf.Tensor] = None,
        function: tp.Optional[tp_loss] = None
        ) -> tf.Tensor:

        assert ypred is not None or x is not None

        if ypred is not None:
            return function(yreal, ypred)
        return function(yreal, self.forward(x))


    def fit_step(
        self,
        inputs: tf.Tensor,
        outputs: tf.Tensor
        ):
        '''
            Runs a fit step, returns the loss and gradients
        '''
        
        with tf.GradientTape() as grads:
            current_loss = self.function(outputs, x=inputs,
                                         function=self.loss_function)

        deltas = grads.gradient(current_loss, self.weights)
        # self.optimizer.apply_gradients(zip(deltas, self.weights))
        return current_loss, deltas


    def fit(
        self,
        epochs: int,
        batch_size: int
        ) -> tp.Dict[str, tp.List[float]]:

        assert self.train_setup, 'First run the training_setup method'

        train_history = {
            'mean_reward':list(),
            'epoch':list(),
            'batch':list(),
            'loss':list()}

        if self.metric_function is not None:
            train_history['metric'] = list()

        if len(self.aux_obs) > 0:
            self.flush_memory()
        mean_reward = tf.reduce_mean(self.rwd_set).numpy()

        for epoch in range(epochs):
            train_set = self.get_train_set()
            batches = train_set.batch(batch_size)
            deltas = list()

            for batch_number, batch in enumerate(batches):

                X, Y = batch

                loss, aux = self.fit_step(X, Y)

                deltas += [aux]

                train_history['mean_reward'] += [mean_reward]
                train_history['epoch'] += [epoch]
                train_history['batch'] += [batch_number]
                train_history['loss'] += [tf.reduce_mean(loss).numpy()]

                if self.metric_function is not None:
                    metric = self.function(Y, x=X,
                        function=self.metric_function)
                    train_history['metric'] += [tf.reduce_mean(metric).numpy()]

            delta = deltas[0]
            for d in deltas[1:]:
                for i, aux in enumerate(d):
                    delta[i] += aux

            self.opt.apply_gradients(zip(delta, self.weights))

        return train_history


    def distilation(
        self,
        X: tf.Tensor,
        Y: tf.Tensor,
        epochs: int,
        batch_size: int
        ) -> None:

        aux = tf.data.Dataset.from_tensor_slices((X, Y))
        train_set = aux.shuffle(
            len(X), reshuffle_each_iteration=True)

        opt = tf.optimizers.RMSprop()

        for epoch in range(epochs):
            batches = train_set.batch(batch_size)
            deltas = list()

            for batch_number, batch in enumerate(batches):
                x, y = batch

                with tf.GradientTape() as grads:
                    loss = tf.keras.losses.mse(
                        y, self.predict_distilation(x))

                delta = grads.gradient(loss, self.weights)

                deltas += [delta]

                lss = tf.reduce_mean(loss).numpy()

            apply_delta = deltas[0]

            for delta in deltas[1:]:
                for i, d in enumerate(delta):
                    apply_delta[i] += d

            opt.apply_gradients(zip(apply_delta, self.weights))


    def reset_train_set(self) -> None:
        '''
            Resets the Train Dataset
        '''
        self.obs_set = None
        self.nxt_set = None
        self.act_set = None
        self.rwd_set = None
        self.end_set = None


    def memorize(
        self,
        obs: tp.List[float],
        nxt_obs: tp.List[float],
        action: int,
        reward: float,
        finish: bool
        ) -> None:
        '''
            Adds a set of observations to the Train Dataset
        '''

        self.aux_obs += [obs]
        self.aux_nxt += [nxt_obs]
        self.aux_act += [[action]]
        self.aux_rwd += [[reward]]
        self.aux_end += [[finish]]

        # tf_obs = tf.constant([obs], dtype=self.tf_dtype)
        # tf_nxt = tf.constant([nxt_obs], dtype=self.tf_dtype)
        # tf_act = tf.constant([[action]], dtype=self.tf_dtype)
        # tf_rwd = tf.constant([[reward]], dtype=self.tf_dtype)
        # tf_end = tf.constant([[finish]], dtype=self.tf_dtype)

        # if self.obs_set is None:
        #     self.obs_set = tf_obs
        #     self.nxt_set = tf_nxt
        #     self.act_set = tf_act
        #     self.rwd_set = tf_rwd
        #     self.end_set = tf_end
        #     return None

        # # diff_obs = tf.reduce_mean(tf.math.abs(self.obs_set - tf_obs), 1)
        # # diff_nxt = tf.reduce_mean(tf.math.abs(self.nxt_set - tf_nxt), 1)
        # # diff_act = tf.reshape(tf.math.abs(self.act_set - tf_act), (-1))
        # # diff_rwd = tf.reshape(tf.math.abs(self.rwd_set - tf_rwd), (-1))
        # # diff_end = tf.reshape(tf.math.abs(self.end_set - tf_end), (-1))

        # # if np.min(diff_obs + diff_nxt + diff_act + diff_rwd + diff_end) > 0:
        # #     # arg = np.argmin(diff_obs + diff_nxt + diff_act + diff_rwd + diff_end)
        # #     # tf_set = tf.constant(
        # #     #     list(set(range(len(self.obs_set))) - set([arg])),
        # #     #     tf.int32)

        # #     # self.obs_set = tf.gather(self.obs_set, tf_set, axis=0)
        # #     # self.nxt_set = tf.gather(self.nxt_set, tf_set, axis=0)
        # #     # self.act_set = tf.gather(self.act_set, tf_set, axis=0)
        # #     # self.rwd_set = tf.gather(self.rwd_set, tf_set, axis=0)
        # #     # self.end_set = tf.gather(self.end_set, tf_set, axis=0)

        # self.obs_set = tf.concat([self.obs_set, tf_obs], 0)
        # self.nxt_set = tf.concat([self.nxt_set, tf_nxt], 0)
        # self.act_set = tf.concat([self.act_set, tf_act], 0)
        # self.rwd_set = tf.concat([self.rwd_set, tf_rwd], 0)
        # self.end_set = tf.concat([self.end_set, tf_end], 0)


    def flush_memory(self):
        tf_obs = tf.constant(self.aux_obs, self.tf_dtype)
        tf_nxt = tf.constant(self.aux_nxt, self.tf_dtype)
        tf_act = tf.constant(self.aux_act, self.tf_dtype)
        tf_rwd = tf.constant(self.aux_rwd, self.tf_dtype)
        tf_end = tf.constant(self.aux_end, self.tf_dtype)

        if self.obs_set is None:
            self.obs_set = tf_obs
            self.nxt_set = tf_nxt
            self.act_set = tf_act
            self.rwd_set = tf_rwd
            self.end_set = tf_end
        else:
            self.obs_set = tf.concat([self.obs_set, tf_obs], 0)
            self.nxt_set = tf.concat([self.nxt_set, tf_nxt], 0)
            self.act_set = tf.concat([self.act_set, tf_act], 0)
            self.rwd_set = tf.concat([self.rwd_set, tf_rwd], 0)
            self.end_set = tf.concat([self.end_set, tf_end], 0)

        mx = self.dataset_size
        if len(self.obs_set) > mx:
            self.obs_set = self.obs_set[-mx:]
            self.nxt_set = self.nxt_set[-mx:]
            self.act_set = self.act_set[-mx:]
            self.rwd_set = self.rwd_set[-mx:]
            self.end_set = self.end_set[-mx:]


    def get_train_set(self) -> tf.data.Dataset:
        X, Y = self.gen_target_set(
            obs = self.obs_set,
            next_obs = self.nxt_set,
            act = self.act_set,
            rwd = self.rwd_set,
            end = self.end_set)

        idx = tf.constant(np.random.permutation(len(self.obs_set)), tf.int32)
        self.obs_set = tf.gather(self.obs_set, idx, axis=0)
        self.nxt_set = tf.gather(self.nxt_set, idx, axis=0)
        self.act_set = tf.gather(self.act_set, idx, axis=0)
        self.rwd_set = tf.gather(self.rwd_set, idx, axis=0)
        self.end_set = tf.gather(self.end_set, idx, axis=0)

        aux = tf.data.Dataset.from_tensor_slices((
            X, Y))

        self.aux_obs = list()
        self.aux_nxt = list()
        self.aux_act = list()
        self.aux_rwd = list()
        self.aux_end = list()

        return aux.shuffle(
            len(self.act_set),
            reshuffle_each_iteration=True)


    def gen_target_set(
        self,
        obs: tf.Tensor,
        next_obs: tf.Tensor,
        act: tf.Tensor,
        rwd: tf.Tensor,
        end: tf.Tensor
        ) -> tp.Tuple[tf.Tensor, tf.Tensor]:
        '''
            Generates a target for the training.
        '''

        y_obs = self.forward(obs)
        y_next_obs = self.forward(next_obs)
        
        max_next_obs = tf.math.reduce_max(y_next_obs, axis=1, keepdims=True)
        max_next_obs *= 1 - end
        max_next_obs *= self.reward_decay_factor
        max_next_obs += rwd
        max_next_obs = tf.reshape(max_next_obs, [-1])

        aux = tf.range(tf.size(act))
        aux = tf.reshape(aux, (-1, 1))
        act = tf.cast(act, tf.int32)
        idx = tf.concat([aux, act], 1)
        
        Y = tf.tensor_scatter_nd_update(y_obs, idx, max_next_obs)

        return obs, Y


    def add_algorithm(
        self,
        N: int
        ) -> None:
        '''
            Adds rate% new neural units.
        '''

        weights = [tf.identity(w) for w in self.weights]

        for l in range(self.model_size - 1):
            weight, bias, weight_ = weights[2*l:2*l+3]

            sizei, perms = weight.shape
            _, sizeo = weight_.shape

            init = tf.keras.initializers.orthogonal()

            for _ in range(N):
                if np.random.random() < 0.5:
                    rnd = np.random.permutation(perms)[:2]

                    bls = np.random.randint(2, size=sizei)
                    bls = np.array([bls, 1 - bls]).T
                    blb = rnd[np.random.randint(2)]

                    w = tf.reduce_sum(
                        tf.gather(weight, rnd, axis=1) * bls, 1, True)
                    b = tf.expand_dims(tf.gather(bias, blb, axis=1), 0)

                    # bls = np.random.randint(2, size=sizeo)
                    # bls = np.array([bls, 1 - bls])

                    # w_ = tf.reduce_sum(
                    #     tf.gather(weight_, rnd, axis=0) * bls, 0, True)
                    # w_ = tf.Variable(tf.zeros((1, sizeo), dtype=self.tf_dtype))

                else:
                    w = tf.Variable(init((sizei, 1), dtype=self.tf_dtype))

                    b = tf.Variable(init((1, 1), dtype=self.tf_dtype))

                    # w_ = tf.Variable(init((1, sizeo), dtype=self.tf_dtype))

                weight = tf.concat([weight, w], 1)
                bias = tf.concat([bias, b], 1)

                w_ = tf.Variable(tf.zeros((1, sizeo), dtype=self.tf_dtype))
                weight_ = tf.concat([weight_, w_], 0)

            weights[2*l] = weight
            weights[2*l+1] = bias
            weights[2*l+2] = weight_

        self.weights = [tf.Variable(w, dtype=self.tf_dtype) for w in weights]


    def remove_algorithm(
        self,
        threshold: float
        ) -> None:
        '''
            Removes the neural units given the threshold
        '''

        current_behavior = self.predict_behavior(self.obs_set, self.weights)

        W = [tf.identity(w) for w in self.weights]
        y = tf.identity(self.obs_set)

        for i in range(self.model_size - 1):
            w = self.weights[2 * i]
            b = self.weights[2 * i + 1]
            y = tf.matmul(y, w) + b
            act = self.activations[i]

            y = act(y)

            z = tf.reduce_mean(y, 0)

            idx = tf.where(z > threshold)[:, 0]

            aux = tf.gather(W[2 * i], idx, axis=1)
            W[2 * i] = aux

            aux = tf.gather(W[2 * i + 1], idx, axis=1)
            W[2 * i + 1] = aux

            aux = tf.gather(W[2 * (i + 1)], idx, axis=0)
            W[2 * (i + 1)] = aux

        self.weights = [tf.Variable(w, dtype=self.tf_dtype) for w in W]

        return self.get_loss(self.weights, current_behavior)


    def combine_neural_units(
        self,
        W: tp.List[tf.Tensor],
        layer: int,
        x: int,
        y: int
        ) -> tp.Tuple[tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor]:

        input_layer = W[2 * layer]
        neural_units_inputs = tf.gather(input_layer, indices=[x, y], axis=1)
        combined_inputs = tf.math.reduce_mean(
            neural_units_inputs,
            axis=1,
            keepdims=True)
        
        bias_layer = W[2 * layer + 1]
        neural_units_bias = tf.gather(bias_layer, indices=[x, y], axis=1)
        combined_bias = tf.math.reduce_mean(
            neural_units_bias,
            axis=1,
            keepdims=True)

        output_layer = W[2 * layer + 2]
        neural_units_outputs = tf.gather(output_layer, indices=[x, y], axis=0)
        combined_outputs = tf.math.reduce_sum(
            neural_units_outputs,
            axis=0,
            keepdims=True)

        return combined_inputs, combined_bias, combined_outputs


    def create_dist_df(self, layer: int) -> pd.DataFrame:

        k = layer

        w = tf.concat([self.weights[2*k], self.weights[2*k+1]], 0)
        df = {
            'i': list(),
            'j': list(),
            'd': list()}

        for i in range(w.shape[1]):
            for j in range(i+1, w.shape[1]):
                d = tf.reduce_sum(tf.abs(w[:, i] - w[:, j])).numpy()

                df['i'] += [i]
                df['j'] += [j]
                df['d'] += [d]

        return pd.DataFrame(df)


    def update_dist_df(self, dist: pd.DataFrame, layer: int) -> pd.DataFrame:

        k = layer
        aux = {'i': list(), 'j': list(), 'd': list()}

        mx = int(dist[['i', 'j']].values.max() + 1)
        w = tf.concat([self.weights[2*k], self.weights[2*k+1]], 0)

        for i in range(w.shape[1]):
            for j in range(mx, w.shape[1]):
                if i >= j:
                    continue
                
                d = tf.reduce_sum(tf.abs(w[:, i] - w[:, j])).numpy()

                aux['i'] += [i]
                aux['j'] += [j]
                aux['d'] += [d]

        aux = pd.DataFrame(aux)
        return dist.append(aux, True)


    def erase_dist_line(self, i: int, dist: pd.DataFrame) -> pd.DataFrame:

        dist = dist[(dist.i != i) & (dist.j != i)].copy()

        dist.loc[dist.i > i, 'i'] -= 1
        dist.loc[dist.j > i, 'j'] -= 1

        return dist


    def get_loss(
        self,
        W: tp.List[tf.Tensor],
        current_behavior: tf.Tensor
        ) -> float:
        
        actual_behavior = self.predict_behavior(self.obs_set, W)

        # N = actual_behavior.shape[1]

        # div = (N - actual_behavior) / tf.math.log(2 + current_behavior)
        # nrm = (N - current_behavior) / tf.math.log(2 + current_behavior)

        # lss = 1 - tf.reduce_sum(div, 1) / tf.reduce_sum(nrm, 1)
        
        # return tf.reduce_mean(lss).numpy()
        return np.mean(tf.losses.mse(current_behavior, actual_behavior))


    def predict_behavior(
        self,
        X: tf.Tensor,
        W: tp.List[tf.Tensor]
        ) -> tf.Tensor:
        
        y = X

        for i, act in enumerate(self.activations):
            w = W[2 * i]
            b = W[2 * i + 1]
            y = tf.matmul(y, w) + b

            y = act(y)

        # return tf.cast(tf.argsort(y, 1, 'DESCENDING'), self.tf_dtype)
        return tf.nn.softmax(y, 1)


    def set_weights(
        self,
        weights: tp.List[np.ndarray],
        void = None
        ) -> None:

        self.weights = [tf.Variable(w, dtype=self.tf_dtype) for w in weights]


    def combination_algorithm(self, threshold: float) -> float:
        current_behavior = self.predict_behavior(self.obs_set, self.weights)

        for l in range(self.model_size - 2, -1, -1):
            W = [tf.identity(w) for w in self.weights]

            dist = self.create_dist_df(l)

            while True:

                i, j, d = dist[dist.d == dist.d.min()].iloc[0]
                i = int(i)
                j = int(j)

                w, b, o = self.combine_neural_units(W, l, i, j)

                idx = list(set(range(W[2 * l].shape[1])) - {i, j})

                W[2 * l] = tf.gather(W[2 * l], idx, axis=1)
                W[2 * l + 1] = tf.gather(W[2 * l + 1], idx, axis=1)
                W[2 * l + 2] = tf.gather(W[2 * l + 2], idx, axis=0)

                W[2 * l] = tf.concat([W[2 * l], w], 1)
                W[2 * l + 1] = tf.concat([W[2 * l + 1], b], 1)
                W[2 * l + 2] = tf.concat([W[2 * l + 2], o], 0)

                new_loss = self.get_loss(W, current_behavior)

                if (new_loss < threshold) and (self.weights[2 * l].shape[1] > 5):
                    k = max(i, j)
                    dist = self.erase_dist_line(k, dist)

                    k = min(i, j)
                    dist = self.erase_dist_line(k, dist)

                    self.weights = [
                        tf.Variable(w, dtype=self.tf_dtype) for w in W]

                    dist = self.update_dist_df(dist, l)

                else:
                    break

        return self.get_loss(self.weights, current_behavior)


    def update_np_weights(self):
        self.np_weights = None
        # self.np_weights = [w.numpy() for w in self.weights]


class AutoEncoder:
    def __init__(
        self,
        inputs: int,
        code: int,
        hidden: tp.List[int],
        activations: tp.List[str],
        tf_dtype: tf.DType = tf.float64
        ) -> None:

        '''
            Note that size of activations has to be:
                acts. = 2 * len(hidden) + 2
        '''

        self.init = tf.initializers.Orthogonal()
        self.tf_dtype = tf_dtype

        self.weights = list()
        inps = inputs
        for outs in hidden + [code] + hidden[::-1] + [inputs]:
            self.weights += self.create_layer(inps, outs)
            inps = outs

        self.model_size = len(activations)
        self.activations = activations


    def create_layer(self, inps: int, outs: int) -> tp.List[tf.Tensor]:
        weights = tf.Variable(self.init((inps, outs), dtype=self.tf_dtype))
        bias = tf.Variable(self.init((1, outs), dtype=self.tf_dtype))
        return [weights, bias]


    def predict_generic(
        self,
        X: tf.Tensor,
        W: tp.List[tf.Tensor],
        A: tp.List[str]
        ) -> tf.Tensor:
        
        Y = tf.identity(X)
        for i in range(len(A)):
            w, b = W[2*i:2*i+2]
            act = tf.keras.activations.get(A[i])

            Y = act(tf.matmul(Y, w) + b)

        return Y


    def predict(self, X: tf.Tensor) -> tf.Tensor:
        return self.predict_generic(X, self.weights, self.activations)


    def fit(
        self,
        X: tf.Tensor,
        epochs: int,
        verbose: bool = False
        ) -> tp.Dict[str, tp.List[float]]:

        train_history = {
            'epoch':list(),
            'batch':list(),
            'loss':list()}

        x = tf.identity(X)

        opt = tf.optimizers.RMSprop()

        for epoch in range(epochs):
            with tf.GradientTape() as grads:
                loss = tf.keras.losses.mse(
                    x, self.predict(x))

            deltas = grads.gradient(loss, self.weights)

            lss = tf.reduce_mean(loss).numpy()

            train_history['epoch'] += [epoch]
            train_history['loss'] += [lss]

            if verbose:
                aprint(f'{epoch} - {lss}', False)

            opt.apply_gradients(zip(deltas, self.weights))

        if verbose:
            aprint('Fim do treino')

        return train_history


    def add_method(self, layer: int, N: int) -> None:
        w, b, w_ = [tf.identity(w) for w in self.weights[2*layer:2*layer+3]]

        sizef, perms = w.shape
        sizep = w_.shape[1]
        
        for _ in range(N):
            if np.random.random() < 0.5:
                rnd = np.random.permutation(perms)[:2]

                bls = np.random.randint(2, size=sizef)
                bls = np.array([bls, 1-bls]).T
                blb = rnd[np.random.randint(2)]

                i = tf.reduce_sum(tf.gather(w, rnd, axis=1) * bls, 1, True)
                j = tf.expand_dims(tf.gather(b, blb, axis=1), 0)

                k = tf.Variable(tf.zeros((1, sizep), dtype=self.tf_dtype))
            else:
                i = tf.Variable(self.init((sizef, 1), dtype=self.tf_dtype))
                j = tf.Variable(self.init((1, 1), dtype=self.tf_dtype))
                k = tf.Variable(tf.zeros((1, sizep), dtype=self.tf_dtype))

            w = tf.concat([w, i], 1)
            b = tf.concat([b, j], 1)
            w_ = tf.concat([w_, k], 0)

        self.weights[2*layer] = tf.Variable(w, dtype=self.tf_dtype)
        self.weights[2*layer+1] = tf.Variable(b, dtype=self.tf_dtype)
        self.weights[2*layer+2] = tf.Variable(w_, dtype=self.tf_dtype)


    def add_algorithm(self, N: int) -> None:
        for l in range(self.model_size - 1):
            self.add_method(l, N)


    def remove_algorithm(self, X: tf.Tensor, threshold: float) -> None:
        Y = tf.identity(X)

        weights = [tf.identity(w) for w in self.weights]

        for l in range(self.model_size - 1):
            w, b, w_ = self.weights[2*l:2*l+3]
            act = tf.keras.activations.get(self.activations[l])

            y = act(tf.matmul(Y, w) + b)
            z = tf.reduce_mean(y, 0) > threshold

            idx = tf.reshape(tf.where(z), (-1)).numpy()
            if len(idx) < 5:
                idx_f = np.random.permutation(
                    tf.reshape(tf.where(
                        tf.math.logical_not(z)), (-1)).numpy())[:5]
                idx = tf.concat([idx, idx_f], 0)

            w = tf.gather(w, idx, axis=1)
            b = tf.gather(b, idx, axis=1)
            w_ = tf.gather(w_, idx, axis=0)

            self.weights[2*l] = tf.Variable(w, dtype=self.tf_dtype)
            self.weights[2*l+1] = tf.Variable(b, dtype=self.tf_dtype)
            self.weights[2*l+2] = tf.Variable(w_, dtype=self.tf_dtype)

            Y = act(tf.matmul(Y, w) + b)


    def create_dist_df(
        self,
        layer: int 
        ) -> pd.DataFrame:

        df = {
            'i': list(),
            'j': list(),
            'd': list()}

        w, b = self.weights[2*layer:2*layer+2]

        for i in range(w.shape[1]):
            for j in range(i+1, w.shape[1]):
                d = np.sum([
                    np.concatenate([
                        np.abs(w[:, i] - w[:, j]).flatten(),
                        np.abs(b[:, i] - b[:, j]).flatten()
                    ])
                ])

                df['i'] += [i]
                df['j'] += [j]
                df['d'] += [d]

        return pd.DataFrame(df)


    def update_dist_df(
        self,
        i: int,
        j: int,
        layer: int,
        df: pd.DataFrame,
        weights: tp.List[tf.Tensor]
        ) -> pd.DataFrame:

        w, b = weights[2*layer:2*layer+2]

        k = max(i, j)

        ds = df[(df.i != k) & (df.j != k)].copy()
        ds['i'] -= 1 * (ds.i > k)
        ds['j'] -= 1 * (ds.j > k)

        k = min(i, j)
        ds = ds[(ds.i != k) & (ds.j != k)].copy()
        ds['i'] -= 1 * (ds.i > k)
        ds['j'] -= 1 * (ds.j > k)

        mx = int(1 + max(ds.i.max(), ds.j.max()))
        ret = {
            'i': list(),
            'j': list(),
            'd': list()}

        for i in range(w.shape[1]):
            for j in range(mx, w.shape[1]):
                if i >= j:
                    continue

                d = np.sum(
                    np.concatenate([
                        np.abs(w[:, i] - w[:, j]).flatten(),
                        np.abs(b[:, i] - b[:, j]).flatten()
                    ]))
                
                ret['i'] += [i]
                ret['j'] += [j]
                ret['d'] += [d]

        return ds.append(pd.DataFrame(ret), True)


    def combine_algorithm(
        self,
        threshold: float,
        X: tf.Tensor,
        epochs: int
        ) -> None:

        weights = [tf.identity(w) for w in self.weights]

        y = self.predict(X)
        error = tf.reduce_mean(tf.losses.mse(X, y))
        if error > threshold:
            return

        for l in reversed(range(self.model_size - 1)):
            # self.fit(X, epochs)

            w, b, w_ = weights[2*l:2*l+3]

            df = self.create_dist_df(l)

            while True:
                i, j, _ = df.iloc[df.d.argmin()]
                i = int(i)
                j = int(j)

                wi = tf.reduce_mean(tf.gather(w, [i, j], axis=1), 1, True)
                bi = tf.reduce_mean(tf.gather(b, [i, j], axis=1), 1, True)
                w_i = tf.reduce_sum(tf.gather(w_, [i, j], axis=0), 0, True)

                idx = list(set(range(w.shape[1])) - {i, j})

                wn = tf.concat([tf.gather(w, idx, axis=1), wi], 1)
                bn = tf.concat([tf.gather(b, idx, axis=1), bi], 1)
                w_n = tf.concat([tf.gather(w_, idx, axis=0), w_i], 0)

                W = [tf.identity(p) for p in weights]
                W[2*l] = wn
                W[2*l+1] = bn
                W[2*l+2] = w_n

                y = self.predict_generic(X, W, self.activations)
                new_error = tf.reduce_mean(tf.losses.mse(X, y))

                if new_error < threshold and w.shape[1] > 5:
                    weights = [tf.identity(p) for p in W]

                    df = self.update_dist_df(i, j, l, df, weights)

                    w = wn
                    b = bn
                    w_ = w_n

                else:
                    break

        self.weights = [tf.Variable(w) for w in weights]


    def encode(self, X: tf.Tensor) -> tf.Tensor:
        return self.predict_generic(
            X,
            self.weights[:self.model_size],
            self.activations[:self.model_size // 2])


    def decode(self, X: tf.Tensor) -> tf.Tensor:
        return self.predict_generic(
            X,
            self.weights[self.model_size:],
            self.activations[self.model_size // 2:])


class Memory:
    def __init__(
        self,
        inputs: int,
        outputs: int,
        hidden: tp.List[int],
        activations: tp.List[str],
        tf_dtype: tf.DType = tf.float64
        ) -> None:

        self.init = tf.initializers.Orthogonal()
        self.tf_dtype = tf_dtype

        self.weights = list()
        inps = inputs
        for outs in hidden + [outputs]:
            self.weights += self.create_layer(inps, outs)
            inps = outs
        
        self.model_size = len(activations)
        self.activations = activations

        self.train_masks = [(0, 0) for _ in range(self.model_size)]
        self.reset_predict_masks()
        
        self.new_model_size = hidden


    def create_layer(self, inps: int, outs: int) -> tp.List[tf.Tensor]:
        weights = tf.Variable(self.init((inps, outs), dtype=self.tf_dtype))
        bias = tf.Variable(self.init((1, outs), dtype=self.tf_dtype))
        return [weights, bias]


    def reset_predict_masks(self) -> None:
        self.predict_masks = [
            tf.constant(tf.ones_like(self.weights[2*i+1])) \
            for i in range(self.model_size)]


    def predict_generic(
        self,
        X: tf.Tensor,
        W: tp.List[tf.Tensor],
        Mask: tp.List[tf.Tensor]
        ) -> tf.Tensor:
        
        Y = tf.identity(X)
        for i in range(self.model_size):
            w, b = W[2*i:2*i+2]
            act = tf.keras.activations.get(self.activations[i])

            Y = act(tf.matmul(Y, w) + b) * Mask[i]

        return Y


    def predict(self, X: tf.Tensor) -> tf.Tensor:
        return self.predict_generic(X, self.weights, self.predict_masks)


    def create_train_masks(self) -> tp.List[tf.Tensor]:
        ret = list()

        for l in range(self.model_size):
            dw = (
                self.weights[2*l].shape[0] - self.train_masks[l][0],
                self.weights[2*l].shape[1] - self.train_masks[l][1])

            zw = tf.zeros(self.train_masks[l], dtype=self.tf_dtype)
            rw = tf.ones((self.train_masks[l][0], dw[1]), dtype=self.tf_dtype)
            bw = tf.ones(
                (dw[0], self.weights[2*l].shape[1]),
                dtype=self.tf_dtype)
            mw = tf.concat([tf.concat([zw, rw], 1), bw], 0)

            zb = tf.zeros((1, self.train_masks[l][1]), dtype=self.tf_dtype)
            rb = tf.ones((1, dw[1]), dtype=self.tf_dtype)
            mb = tf.concat([zb, rb], 1)

            ret += [tf.constant(mw), tf.constant(mb)]

        return ret


    def fit(
        self,
        X: tf.Tensor,
        Y: tf.Tensor,
        epochs: int,
        batch_size: int,
        verbose: bool = False
        ) -> tp.Dict[str, tp.List[float]]:

        train_masks = self.create_train_masks()

        train_history = {
            'epoch':list(),
            'batch':list(),
            'loss':list()}

        aux = tf.data.Dataset.from_tensor_slices((X, Y))
        train_set = aux.shuffle(
            len(X), reshuffle_each_iteration=True)

        opt = tf.optimizers.RMSprop()

        for epoch in range(epochs):
            batches = train_set.batch(batch_size)
            deltas = list()

            for batch_number, batch in enumerate(batches):
                x, y = batch

                with tf.GradientTape() as grads:
                    loss = tf.keras.losses.mse(
                        y, self.predict(x))

                delta = grads.gradient(loss, self.weights)

                deltas += [delta]

                lss = tf.reduce_mean(loss).numpy()

                train_history['epoch'] += [epoch]
                train_history['batch'] += [batch_number]
                train_history['loss'] += [lss]

                if verbose:
                    aprint(f'{epoch} - {batch_number} - {lss}', False)

            apply_delta = deltas[0]

            for delta in deltas[1:]:
                for i, d in enumerate(delta):
                    apply_delta[i] += d

            for i, mask in enumerate(train_masks):
                apply_delta[i] = apply_delta[i] * mask

            opt.apply_gradients(zip(apply_delta, self.weights))

        if verbose:
            aprint('Fim do treino')

        return train_history


    def add_method(self, layer: int, N: int) -> None:
        w, b, w_ = [tf.identity(w) for w in self.weights[2*layer:2*layer+3]]

        sizef, perms = w.shape
        sizep = w_.shape[1]
        
        for _ in range(N):
            if np.random.random() < 0.5:
                rnd = np.random.permutation(perms)[:2]

                bls = np.random.randint(2, size=sizef)
                bls = np.array([bls, 1-bls]).T
                blb = rnd[np.random.randint(2)]

                i = tf.reduce_sum(tf.gather(w, rnd, axis=1) * bls, 1, True)
                j = tf.expand_dims(tf.gather(b, blb, axis=1), 0)

                k = tf.Variable(tf.zeros((1, sizep), dtype=self.tf_dtype))
            else:
                i = tf.Variable(self.init((sizef, 1), dtype=self.tf_dtype))
                j = tf.Variable(self.init((1, 1), dtype=self.tf_dtype))
                k = tf.Variable(tf.zeros((1, sizep), dtype=self.tf_dtype))

            w = tf.concat([w, i], 1)
            b = tf.concat([b, j], 1)
            w_ = tf.concat([w_, k], 0)

            self.predict_masks[layer] = tf.concat([
                self.predict_masks[layer],
                tf.ones((1, 1), dtype=self.tf_dtype)], 1)

        self.weights[2*layer] = tf.Variable(w, dtype=self.tf_dtype)
        self.weights[2*layer+1] = tf.Variable(b, dtype=self.tf_dtype)
        self.weights[2*layer+2] = tf.Variable(w_, dtype=self.tf_dtype)


    def add_algorithm(self, N: int) -> None:
        for l in range(self.model_size - 1):
            # N = int(np.ceil(rate * self.weights[2*l].shape[1]))
            self.add_method(l, N)


    def add_model(self) -> None:
        for l, N in enumerate(self.new_model_size):
            self.add_method(l, N)


    def remove_algorithm(self, X: tf.Tensor, threshold: float) -> None:
        Y = tf.identity(X)

        weights = [tf.identity(w) for w in self.weights]

        for l in range(self.model_size - 1):
            w, b, w_ = self.weights[2*l:2*l+3]
            act = tf.keras.activations.get(self.activations[l])

            y = act(tf.matmul(Y, w) + b) * self.predict_masks[l]
            z = tf.reduce_mean(y, 0) > threshold  # [1101 1010]

            idx_k = set(tf.reshape(tf.where(z), (-1)).numpy())  # (01346)
            idx_f = set(range(self.train_masks[l][1]))  # (0123)
            idx = list(idx_f.union(idx_k))  # (012346)
            idx_p = list(idx_f.intersection(idx_k))  # (013)
            idx_p += list(range(self.train_masks[l][1], len(idx)))  # (01345)

            w = tf.gather(w, idx, axis=1)
            b = tf.gather(b, idx, axis=1)
            w_ = tf.gather(w_, idx, axis=0)

            updates = tf.ones((len(idx_k)), dtype=self.tf_dtype)
            indices = tf.transpose(tf.constant([idx_p]))
            self.predict_masks[l] = tf.reshape(
                tf.scatter_nd(indices, updates, (len(idx),)), (1, -1))
            
            self.weights[2*l] = tf.Variable(w, dtype=self.tf_dtype)
            self.weights[2*l+1] = tf.Variable(b, dtype=self.tf_dtype)
            self.weights[2*l+2] = tf.Variable(w_, dtype=self.tf_dtype)

            Y = act(tf.matmul(Y, w) + b) * self.predict_masks[l]


    def behavior_error(
        self,
        y_true: tf.Tensor,
        y_pred: tf.Tensor
        ) -> float:

        # Yb = tf.cast(tf.argsort(y_true, 1, 'DESCENDING'), tf.float64)
        # yb = tf.cast(tf.argsort(y_pred, 1, 'DESCENDING'), tf.float64)

        # N = Yb.shape[1]

        # div = (N - yb) / tf.math.log(2 + Yb)
        # nrm = (N - Yb) / tf.math.log(2 + Yb)

        # lss = 1 - tf.reduce_sum(div, 1) / tf.reduce_sum(nrm, 1)

        # return tf.reduce_mean(lss).numpy()
        return np.mean(tf.losses.mse(y_true, y_pred))


    def create_dist_df(
        self,
        layer: int 
        ) -> pd.DataFrame:

        df = {
            'i': list(),
            'j': list(),
            'd': list()}

        w, b = self.weights[2*layer:2*layer+2]
        blocked = self.train_masks[layer][1]

        # for i in range(blocked, w.shape[1]):
            # for j in range(blocked + 1, w.shape[1]):
        for i in range(w.shape[1]):
            for j in range(blocked, w.shape[1]):
                if i >= j:
                    continue

                d = np.sum([
                    np.concatenate([
                        np.abs(w[:, i] - w[:, j]).flatten(),
                        np.abs(b[:, i] - b[:, j]).flatten()
                    ])
                ])

                df['i'] += [i]
                df['j'] += [j]
                df['d'] += [d]

        return pd.DataFrame(df)


    def update_dist_df(
        self,
        i: int,
        j: int,
        layer: int,
        df: pd.DataFrame,
        weights: tp.List[tf.Tensor]
        ) -> pd.DataFrame:

        w, b = weights[2*layer:2*layer+2]
        blocked = self.train_masks[layer][1]

        k = max(i, j)
        if k < blocked:
            ds = df.copy()
        else:
            ds = df[(df.i != k) & (df.j != k)].copy()
            ds['i'] -= 1 * (ds.i > k)
            ds['j'] -= 1 * (ds.j > k)

        k = min(i, j)
        if k >= blocked:
            ds = ds[(ds.i != k) & (ds.j != k)].copy()
            ds['i'] -= 1 * (ds.i > k)
            ds['j'] -= 1 * (ds.j > k)

        if len(ds) == 0:
            return ds

        mx = int(1 + ds[['i', 'j']].values.max())
        ret = {
            'i': list(),
            'j': list(),
            'd': list()}

        # for i in range(blocked, w.shape[1]):
        for i in range(w.shape[1]):
            for j in range(mx, w.shape[1]):
                if i >= j:
                    continue

                d = np.sum(
                    np.concatenate([
                        np.abs(w[:, i] - w[:, j]).flatten(),
                        np.abs(b[:, i] - b[:, j]).flatten()
                    ]))
                
                ret['i'] += [i]
                ret['j'] += [j]
                ret['d'] += [d]

        return ds.append(pd.DataFrame(ret), True)


    def combine_algorithm(
        self,
        threshold: float,
        X: tf.Tensor,
        Y: tf.Tensor,
        epochs: int,
        batch_size: int
        ) -> None:

        weights = [tf.identity(w) for w in self.weights]
        predict_masks = [tf.identity(w) for w in self.predict_masks]

        y = self.predict(X)
        current_behavior = self.behavior_error(Y, y)
        if current_behavior > threshold:
            return

        for l in reversed(range(self.model_size - 1)):
            # self.fit(X, Y, epochs, batch_size)

            w, b, w_ = weights[2*l:2*l+3]
            blocked = self.train_masks[l][1]
            m = predict_masks[l]

            df = self.create_dist_df(l)

            count = 0

            while True:
                if len(df) == 0:
                    break

                i, j, _ = df.iloc[df.d.argmin()]
                i = int(i)
                j = int(j)

                if i < blocked:
                    idx = list(set(range(w.shape[1])) - {j})

                    wn = tf.gather(w, idx, axis=1)
                    bn = tf.gather(b, idx, axis=1)
                    w_n = tf.gather(w_, idx, axis=0)

                    mn = tf.gather(m, idx, axis=1)

                else:
                    wi = tf.reduce_mean(tf.gather(w, [i, j], axis=1), 1, True)
                    bi = tf.reduce_mean(tf.gather(b, [i, j], axis=1), 1, True)
                    w_i = tf.reduce_sum(tf.gather(w_, [i, j], axis=0), 0, True)

                    idx = list(set(range(w.shape[1])) - {i, j})

                    wn = tf.concat([tf.gather(w, idx, axis=1), wi], 1)
                    bn = tf.concat([tf.gather(b, idx, axis=1), bi], 1)
                    w_n = tf.concat([tf.gather(w_, idx, axis=0), w_i], 0)

                    mn = tf.concat([
                        tf.gather(m, idx, axis=1),
                        tf.ones((1, 1), dtype=self.tf_dtype)], 1)

                W = [tf.identity(p) for p in weights]
                W[2*l] = wn
                W[2*l+1] = bn
                W[2*l+2] = w_n

                M = [tf.identity(p) for p in predict_masks]
                M[l] = mn

                y = self.predict_generic(X, W, M)
                new_behavior = self.behavior_error(Y, y)

                # aprint(f'{l}: {i}x{j} - {wn.shape} - {new_behavior}', False)

                if (new_behavior < threshold) and (weights[2*l].shape[1] > 5):
                    weights = [tf.identity(p) for p in W]
                    predict_masks = [tf.identity(p) for p in M]

                    df = self.update_dist_df(i, j, l, df, weights)
                    
                    if len(df) == 0:
                        break

                    w = wn
                    b = bn
                    w_ = w_n
                    m = mn

                    # aprint(f'Combinado {l}: {i}x{j} - {new_behavior}')
                    count = 0

                else:
                    # break
                    df = df[(df.i != i) | (df.j != j)]
                    count += 1

                if count == 10:
                    break

                # if i == count[1] and j == count[2]:
                #     count[0] += 1
                # else:
                #     count = [0, i, j]

                # if count[0] > 5:
                #     assert False, "Something is still wrong here!"

        self.weights = [tf.Variable(w) for w in weights]
        self.predict_masks = [tf.Variable(w) for w in predict_masks]


    def consolidate_model(self) -> None:
        self.train_masks = [
            self.weights[2*l].shape for l in range(self.model_size)]


class selector:
    def __init__(
        self,
        inputs: int,
        outputs: int,
        tf_dtype: tf.DType = tf.float64
        ) -> None:

        self.tf_dtype = tf_dtype
        self.create_weights(inputs, outputs)


    def create_weights(self, inputs: int, outputs: int) -> None:
        init = tf.initializers.orthogonal()

        self.weights = tf.Variable(init((inputs, outputs), dtype=self.tf_dtype))
        self.bias = tf.Variable(init((1, outputs), dtype=self.tf_dtype))


    def predict(self, X: tf.Tensor) -> tf.Tensor:
        w = self.weights
        b = self.bias
        act = tf.keras.activations.get('sigmoid')

        return act(tf.matmul(X, w) + b)


    def predict_selector(self, X: tf.Tensor, threshold: float) -> tf.Tensor:
        y = self.predict(X)
        return tf.cast(y > threshold, dtype=self.tf_dtype)


    def fit(
        self,
        X: tf.Tensor,
        Y: tf.Tensor,
        epochs: int
        ) -> tp.Dict[str, tp.List[float]]:

        train_history = {
            'epoch':list(),
            'loss':list()}

        weights_aux = [self.weights, self.bias]

        opt = tf.optimizers.RMSprop()

        for epoch in range(epochs):
            with tf.GradientTape() as grads:
                loss = tf.keras.losses.mse(
                    Y, self.predict(X))

                delta = grads.gradient(loss, weights_aux)

                train_history['epoch'] += [epoch]
                train_history['loss'] += [tf.reduce_mean(loss).numpy()]

            opt.apply_gradients(zip(delta, weights_aux))

        return train_history


class agent: 
    def __init__(
        self,
        vanilla: bool = False,
        **params: tp.Dict[str, any]
        ) -> None:
        '''
            params: dicionário de parâmetros
                agent_tf_dtype: tf.dtype, default = tf.float64
                agent_code: str

                environment_class: Instance of an Environment

                arcnet_topology: List[int]
                arcnet_activations: List[str]
                arcnet_gamma: float [0, 1]
                arcnet_iterations: int [1,)
                arcnet_batchsize: int [1,)
                arcnet_addrate: float (0,)
                arcnet_epochs: int [1,)
                arcnet_trainepochs: int [1,)
                arcnet_episodes: int [1,)
                arcnet_breakpoint: int [1,)
                arcnet_removethreshold: float [0, 1]
                arcnet_combinelimit: float [0, 1]
                arcnet_verbose: bool
                arcnet_datasetsize: int [1,)
                arcnet_runs: List[int]

                ae_topology: List[int]
                ae_code: int [1,)
                ae_activations: List[str]
                ae_iterations: int [1,)
                ae_addrate: float [0, 1]
                ae_trainepochs: int [1,)
                ae_trainthreshold: float [0,)
                ae_epochs: int [1,)
                ae_removethreshold: float [0, 1]
                ae_combinethreshold: float [0, 1]

                memory_topology: List[int]
                memory_activations: List[str]
                memory_iterations: int [1,)
                memory_add_rate: float [0, 1]
                memory_epochs: int [1,)
                memory_trainepochs: int [1,)
                memory_breakpoint: int [1,)
                memory_batch_size: int [1,)
                memory_train_accuracy: float [0, 1]
                memory_remove_threshold: float [0, 1]
                memory_combine_threshold: float [0, 1]

                selector_threshold: float [0, 1]
                selector_epochs: int [1,)
                selector_error: float [0,)
                selector_tries: int [1,)
        '''

        missing = list()
        for n in [
            'agent_code',
            'environment_class',
            'arcnet_topology',
            'arcnet_activations',
            'arcnet_gamma',
            'arcnet_iterations',
            'arcnet_batchsize',
            'arcnet_addrate',
            'arcnet_epochs',
            'arcnet_trainepochs',
            'arcnet_episodes',
            'arcnet_breakpoint',
            'arcnet_removethreshold',
            'arcnet_combinelimit',
            'arcnet_verbose',
            'arcnet_datasetsize',
            'arcnet_runs',
            'ae_topology',
            'ae_code',
            'ae_activations',
            'ae_iterations',
            'ae_addrate',
            'ae_trainepochs',
            'ae_trainthreshold',
            'ae_epochs',
            'ae_removethreshold',
            'ae_combinethreshold',
            'memory_topology',
            'memory_activations',
            'memory_iterations',
            'memory_add_rate',
            'memory_epochs',
            'memory_trainepochs',
            'memory_batch_size',
            'memory_breakpoint',
            'memory_train_accuracy',
            'memory_remove_threshold',
            'memory_combine_threshold',
            'selector_threshold',
            'selector_epochs',
            'selector_error',
            'selector_tries']:

            if n not in params:
                missing += [n]

        if len(missing) > 1:
            string = ', '.join(missing[0:-1])
            string += ' and ' + missing[-1]
            raise Exception(f'The following params are missing: {string}')
        elif len(missing) == 1:
            raise Exception(f'Tha param {missing[0]} is missing')

        for k, v in params.items():
            setattr(self, k, v)

        if 'agent_tf_dtype' not in params:
            self.agent_tf_dtype = tf.float64

        if 'agent_agent_decisionvector' not in params:
            self.agent_decisionvector = None

        self.set_env(self.environment_class)
        self.reset(True)
        self.create_autoencoder()
        self.env_vectors = list()
        self.create_memory()
        self.selector_olders = None

        self.agent_memory = False

        self.safe = list()

        self.safe_weights = None
        self.safe_vars = None
        self.safe_activations = None

        self.vanilla = vanilla
        self.arcnet_current_run = None


    def set_env(self, env: any, show: bool = False) -> None:
        self.environment_class = env
        self.env = self.environment_class
        self.env.reset()
        if show:
            env.show()


    def reset(self, arcnet=False) -> None:
        '''
            Reseta o env e o modelo
        '''

        self.env = self.environment_class
        self.env.reset()
        inputs = self.env.observation_space
        outputs = self.env.action_space

        if not arcnet:
            return None

        # def func(x, y):
        #     aux = tf.where(y > 0, x + 0.05 * (1 - x), x - 0.05 * x)
        #     return tf.reshape(aux, x.shape)

        self.modelo = ARCNet(
            inputs, outputs,
            self.arcnet_topology,
            self.arcnet_activations,
            # func,
            self.arcnet_datasetsize,
            self.agent_tf_dtype)


    def episode(self) -> None:
        '''
            Roda um episódio, armazenando os dados no Dataset do modelo
        '''

        self.env.reset()
        rwds = list()

        for cnt in range(self.arcnet_episodes):
            obs = self.env.obs()

            rnd = np.random.choice(
                [True, False],
                p = self.agent_decisionvector / self.agent_decisionvector.sum())

            if rnd:
                # if self.agent_memory and not self.vanilla:
                #     decision = 1
                # else:
                decision = 0
            else:
                decision = 2

            if decision == 0:
                act = self.random_decision()
            # elif decision == 1:
            #     X = tf.constant([obs], self.agent_tf_dtype)
            #     Y = self.memory.predict(X)
            #     act = self.random_decision(Y)
            else:
                X = tf.constant([obs], self.agent_tf_dtype)
                Y = self.modelo.predict(X)
                act = int(np.argmax(Y, 1))

            self.action_vector[act] += 1
            rwd, finish = self.env.act(act)
            rwds += [rwd]

            aprint(f'episode: {cnt / self.arcnet_episodes}', False)

            # aux = 1 / self.arcnet_episodes
            # rnd = np.random.random() < aux

            # if finish or rnd:
            #     if self.agent_decisionvector[1] <= 7 - aux:
            #         self.agent_decisionvector[1] += aux
            #     if self.agent_decisionvector[0] >= 1 + aux:
            #         self.agent_decisionvector[0] -= aux
            # elif rnd:
            #     if self.agent_decisionvector[0] <= 7 - aux:
            #         self.agent_decisionvector[0] += aux

            next_obs = self.env.obs()

            self.modelo.memorize(obs, next_obs, act, rwd, finish)

            if finish:
                self.env.reset()

        return rwds


    def random_decision(self, values: tf.Tensor = None) -> int:
        if values is None:
            return np.random.randint(self.env.action_space)

        # x = np.exp(values)
        # y = x / np.sum(x)
        y = values.numpy()

        return np.random.choice(self.env.action_space, p = y.flatten())


    def episodes(
        self,
        N: tp.Optional[int] = None
        ) -> None:
        '''
            N: quantidade de episódios que devem ser executados
        '''
        # if N is None:
            # N = self.arcnet_episodes

        # self.modelo.reset_train_set()
        self.modelo.update_np_weights()
        # self.env.heat_map = np.ones((11, 11))
        self.action_vector = np.ones(self.env.action_space)

        # rwd = list()
        # for _ in range(N):
            # rwd += self.episode()
        rwd = self.episode()
        return np.mean(rwd)


    def train_model(
        self,
        epochs: tp.Optional[int] = None,
        batch_size: tp.Optional[int] = None
        ) -> tp.Dict[str, float]:
        '''
            Treina o modelo, utilizando os dados armazenados
            
            epochs: quantidade de épocas a serem executadas
            batch_size: tamanho do batch de treinamento

            Retorna o histórico de scores do treinamento executado
        '''

        if epochs is None:
            epochs = self.arcnet_epochs
        if batch_size is None:
            batch_size = self.arcnet_batchsize
        
        loss = tf.losses.mse
        self.modelo.training_setup(loss, gamma=self.arcnet_gamma)
        return self.modelo.fit(epochs, batch_size)


    def env_test(self) -> float:
        '''
            Testa o modelo a partir da interação com o ambiente
        '''
        lv = self.env.level
        X = tf.constant(np.load(f'{code_folder}/X{lv - 1}.npy'), dtype=self.agent_tf_dtype)
        Y = np.load(f'{code_folder}/Y{lv - 1}.npy')

        yp = self.modelo.predict(X).numpy()

        yz = np.exp(yp) / np.sum(np.exp(yp), 1, keepdims=True)
        out = np.sum(Y * yz) / 50

        # out = 0
        # mn = 10
        # for i, a in enumerate(tf.argmax(yp, 1)):
        #     aux = Y[i, a]
        #     out += aux
        #     mn = min(mn, aux)

        # return out / 50, mn >= 9
        return out


    def env_recall(self, memory_ready=True) -> float:
        lv = self.env.level
        X = tf.constant(
            np.load(f'{code_folder}/X{lv-1}.npy'),
            dtype=self.agent_tf_dtype)
        Y = np.load(f'{code_folder}/Y{lv-1}.npy')

        if not memory_ready:
            z = tf.constant(
                self.env_description(),
                self.agent_tf_dtype)
            z = self.ae.encode(z)

            masks = [s.predict_selector(
                z, self.selector_threshold) for s in self.selectors]
            masks += [tf.ones_like(self.memory.weights[-1])]

            self.memory.predict_masks = masks

        y = self.memory.predict(X).numpy()
        return np.sum(Y * y) / 50


    def env_description(self) -> tf.data.Dataset:
        '''
            Cria um data set que descreve o modelo

            Retorna o Dataset criado
        '''

        vec = list()
        poss = [
            [0, 0, 0, 0, 0],
            [0, 6, 0, 8, 0],
            [0, 6, 7, 0, 13],
            [0, 6, 7, 8, 0],
            [0, 6, 7, 8, 13]][self.env.level - 1]

        tgts = [
            [[6], [7], [8], [13], [16]],
            [[6, 7], [6, 7], [8, 13], [8, 13], [16, 4]],
            [[6, 7, 8], [6, 7, 8], [6, 7, 8], [13, 16, 4], [13, 16, 4]],
            [[6, 7, 8, 13], [6, 7, 8, 13], [6, 7, 8, 13], [6, 7, 8, 13], [16, 4, 18, 17]],
            [[6, 7, 8, 13, 16], [6, 7, 8, 13, 16], [6, 7, 8, 13, 16], [6, 7, 8, 13, 16], [6, 7, 8, 13, 16]]
            ][self.env.level - 1]

        stps = [
            [[], [], [], [], []],
            [[], [6], [], [8], []],
            [[], [6], [6, 7], [], [13]],
            [[], [6], [6, 7], [6, 7, 8], []],
            [[], [6], [6, 7], [6, 7, 8], [6, 7, 8, 13]]][self.env.level - 1]

        for i in range(5):
            self.env.pos = poss[i]
            self.env.target = np.array(tgts[i])
            self.env.stamps = stps[i]
            vec += self.env.obs()

        return tf.constant([vec], dtype=self.agent_tf_dtype)


    def train_arcnet(
        self,
        iterations: tp.Optional[int] = None,
        batch_size: tp.Optional[int] = None,
        add_rate: tp.Optional[float] = None,
        runs: tp.Optional[int] = None,
        epochs: tp.Optional[int] = None,
        episodes: tp.Optional[int] = None,
        remove_threshold: tp.Optional[float] = None,
        combine_limit: tp.Optional[float] = None,
        verbose: tp.Optional[bool] = None,
        scene: int = 0,
        initial_iteration: int = 0
        ) -> None:

        if iterations is None:
            iterations = self.arcnet_iterations
        if batch_size is None:
            batch_size = self.arcnet_batchsize
        if add_rate is None:
            add_rate = self.arcnet_addrate
        if runs is None:
            runs = self.arcnet_epochs
        if epochs is None:
            epochs = self.arcnet_trainepochs
        if episodes is None:
            episodes = self.arcnet_episodes
        if remove_threshold is None:
            remove_threshold = self.arcnet_removethreshold
        if combine_limit is None:
            combine_limit = self.arcnet_combinelimit
        if verbose is None:
            verbose = self.arcnet_verbose

        self.modelo.get_best = True

        # for iteration in range(initial_iteration, iterations):
        tsi = time.time()
        # if self.agent_decisionvector is None:
        #     self.agent_decisionvector = np.array([7., 1.])

        # if self.safe_weights is None:
        #     self.safe_weights = [
        #         tf.identity(w) for w in self.modelo.weights]

        # if self.modelo.train_setup and iteration > 0:
        #     self.modelo.add_algorithm(add_rate)

        #     shape = [w.shape[1] for w in self.modelo.weights[::2]]
        #     self.to_safe(scene, 'arcnet', 'add', iteration, shape, 0, 0)

        cnt = 0
        rst_cnt = 0

        epoch = 0
        ends = self.env.endings

        while self.arcnet_current_run > 0:
            self.arcnet_current_run -= 1

            self.modelo.opt = tf.optimizers.RMSprop()
            before = self.env.endings

            x = self.arcnet_current_run / self.arcnet_runs[scene]
            self.agent_decisionvector = np.clip([15 * x + 0.25, -15 * x + 15.25], 1, 7)
            if self.agent_memory and not self.vanilla:
                distillation_epochs = int(np.clip(
                    250 - 10 * (self.arcnet_runs[scene] - self.arcnet_current_run), 0, 200))

            ts = time.time()
            mean_rwd = self.episodes(episodes)
            if self.agent_memory and not self.vanilla and distillation_epochs > 0:
                self.modelo.flush_memory()
                Y = self.memory.predict(self.modelo.obs_set)
                self.modelo.distilation(
                    self.modelo.obs_set, Y,
                    distillation_epochs,
                    self.arcnet_batchsize)
            self.train_model(epochs, batch_size)
            ts = time.time() - ts

            shape = [w.shape[1] for w in self.modelo.weights[::2]]
            score = self.env_test()

            if (self.arcnet_current_run % 10 == 0):
                self.to_safe(
                    scene, 'arcnet', 'train', initial_iteration, shape, mean_rwd,
                    ts, self.env.endings, score)
                if self.arcnet_current_run > 0:
                    self.force_arcnet(scene)
                self.train_model(epochs, batch_size)
                self.save_me(self.agent_code)
            else:
                self.to_safe(
                    scene, 'arcnet', 'train', initial_iteration, shape, mean_rwd,
                    ts, self.env.endings, score)

            # envtest, cond0 = 
            # cond1 = envtest > 9

            # if cond1 and cond0:
            #     cnt += 1
            # else:
            #     cnt = 0

            # if self.env.endings == before:
            #     rst_cnt += 1
            # else:
            #     rst_cnt = 0

            # cond2 = cnt >= self.arcnet_breakpoint
            # cond3 = self.env.endings - ends >= epoch
            # cond4 = epoch >= runs

            if verbose:
                aux = f'endings {before} -> {self.env.endings}'
                aux += f' : {self.arcnet_current_run}'
                aprint(aux)

            # plt.imshow(self.env.heat_map)
            # plt.show()

            # plt.bar(np.arange(9), self.action_vector)
            # plt.show()

            aprint(f'Random: {np.round(self.agent_decisionvector, 2)}')

            # if cond1 and cond2:
            #     break

            # cond5 = rst_cnt >= runs
            # if cond5:
            #     rst_cnt = 0
            #     initial_iteration += 1
            #     self.force_arcnet(scene)

                # self.modelo.remove_algorithm(remove_threshold)
                # shape = [w.shape[1] for w in self.modelo.weights[::2]]
                # self.to_safe(scene, 'arcnet', 'remove', 0, shape, 0, 0)

                # self.modelo.combination_algorithm(combine_limit)
                # shape = [w.shape[1] for w in self.modelo.weights[::2]]
                # self.to_safe(scene, 'arcnet', 'combine', 0, shape, 0, 0)

                # self.modelo.add_algorithm(add_rate)
                # shape = [w.shape[1] for w in self.modelo.weights[::2]]
                # self.to_safe(scene, 'arcnet', 'add', 0, shape, 0, 0)
                # self.save_me(self.agent_code)

            # cond6 = self.env.endings > 0
            # cond7 = rst_cnt >= 5 * self.arcnet_breakpoint
            # cond8 = epochs >= 5 * runs
            # if not cond1 and (cond6 or cond8) and cond7:
            #     rst_cnt = 0
            #     shape = [w.shape[1] for w in self.modelo.weights[::2]]
            #     self.to_safe(
            #         scene, 'arcnet', 'train_reset', iteration, shape, 0, 0)
            #     self.modelo.weights = [tf.Variable(
            #         w, dtype = self.agent_tf_dtype
            #         ) for w in self.safe_weights]
            #     self.modelo.activation_factor = [
            #         tf.Variable(a,
            #             dtype=self.agent_tf_dtype) \
            #         for a in self.safe_activations]
            #     self.modelo.opt = tf.optimizers.RMSprop()

        self.modelo.opt = None
        # shape = [w.shape[1] for w in self.modelo.weights[::2]]
        # self.to_safe(
        #     scene, 'arcnet', 'train_finished', iteration, shape, 0, 0)

        # try:
        #     self.modelo.remove_algorithm(remove_threshold)
        # except RottedModelException as e:
        #     aprint(e)
        #     shape = [w.shape[1] for w in self.modelo.weights[::2]]
        #     self.to_safe(
        #         scene, 'arcnet', 'remove_reset', iteration, shape, 0, 0)

        #     self.modelo.weights = [tf.Variable(
        #         w, dtype = self.agent_tf_dtype) for w in self.safe_weights]

        # else:
        #     shape = [w.shape[1] for w in self.modelo.weights[::2]]
        #     self.to_safe(scene, 'arcnet', 'remove', iteration, shape, 0, 0)

        #     self.modelo.combination_algorithm(combine_limit)
        #     shape = [w.shape[1] for w in self.modelo.weights[::2]]
        #     self.to_safe(scene, 'arcnet', 'combine', iteration, shape, 0, 0)

            # self.modelo.opt = tf.optimizers.RMSprop()
                # self.train_model(epochs, batch_size)
                # self.modelo.opt = None

        # self.safe_weights = None

        tsi = time.time() - tsi
        shape = [w.shape[1] for w in self.modelo.weights[::2]]
        self.agent_decisionvector = None
        self.to_safe(scene, 'arcnet', 'finished', 0, shape, 0, tsi)
        self.save_me(self.agent_code)


    def force_arcnet(self, scene: int) -> None:
        add_rate = self.arcnet_addrate
        remove_threshold = self.arcnet_removethreshold
        combine_limit = self.arcnet_combinelimit

        ts = time.time()
        aux = self.modelo.remove_algorithm(remove_threshold)
        ts = time.time() - ts
        shape = [w.shape[1] for w in self.modelo.weights[::2]]
        score = self.env_test()
        self.to_safe(scene, 'arcnet', 'remove', 0, shape, aux, ts, 0, score)

        ts = time.time()
        aux = self.modelo.combination_algorithm(combine_limit)
        ts = time.time() - ts
        shape = [w.shape[1] for w in self.modelo.weights[::2]]
        score = self.env_test()
        self.to_safe(scene, 'arcnet', 'combine', 0, shape, aux, ts, 0, score)

        self.modelo.add_algorithm(add_rate)
        shape = [w.shape[1] for w in self.modelo.weights[::2]]
        score = self.env_test()
        self.to_safe(scene, 'arcnet', 'add', 0, shape, 0, 0, 0, score)
        # self.save_me(self.agent_code)


    def create_autoencoder(self) -> None:
        layers = self.ae_topology
        code = self.ae_code
        acts = self.ae_activations

        _, inputs = self.env_description().shape

        self.ae = AutoEncoder(inputs, code, layers, acts, self.agent_tf_dtype)


    def train_autoencoder(
        self,
        scene: int = 0,
        initial_iteration: int = 0
        ) -> None:

        X = tf.constant(
            self.env_description(),
            dtype=self.agent_tf_dtype)

        if len(self.env_vectors) > 0:
            Xc = tf.identity(self.env_vectors)
            Xc = tf.round(self.ae.decode(Xc))

            X = tf.concat([X, Xc], 0)

        self.create_autoencoder()

        for iteration in range(initial_iteration, self.ae_iterations):
            tsi = time.time()

            if iteration > 0:
                self.ae.add_algorithm(self.ae_addrate)
                shape = [w.shape[1] for w in self.ae.weights[::2]]
                self.to_safe(scene, 'ae', 'add', iteration, shape, 0, 0)

            for i in range(self.ae_trainepochs):
                ts = time.time()
                self.ae.fit(X, self.ae_epochs)
                ts = time.time() - ts

                Y = self.ae.predict(X)
                loss = tf.reduce_mean(tf.losses.mse(X, Y))
                shape = [w.shape[1] for w in self.ae.weights[::2]]
                self.to_safe(scene, 'ae', 'train', iteration, shape, loss, ts)

                if loss < self.ae_trainthreshold:
                    break

            self.ae.remove_algorithm(X, self.ae_removethreshold)
            shape = [w.shape[1] for w in self.ae.weights[::2]]
            self.to_safe(scene, 'ae', 'remove', iteration, shape, 0, 0)

            self.ae.combine_algorithm(
                self.ae_combinethreshold, X, self.ae_epochs)
            Y = self.ae.predict(X)
            loss = tf.reduce_mean(tf.losses.mse(X, Y))
            shape = [w.shape[1] for w in self.ae.weights[::2]]
            self.to_safe(scene, 'ae', 'combine', iteration, shape, loss, 0)

            # self.ae.fit(X, self.ae_epochs)

            tsi = time.time() - tsi
            Y = self.ae.predict(X)
            loss = tf.reduce_mean(tf.losses.mse(X, Y))
            self.to_safe(scene, 'ae', 'finished', iteration, shape, loss, tsi)
            self.save_me(self.agent_code)

        self.remake_env_vectors(X)


    def gen_env_vector(self) -> tf.Tensor:
        V = self.env_description()
        Y = tf.constant(V, dtype=self.agent_tf_dtype)

        Y = self.ae.encode(Y)

        self.env_vectors += [Y]

        return Y


    def reconstruct_env_vectors(self) -> tf.Tensor:
        Y = tf.concat(self.env_vectors, 0)

        X = tf.identity(Y)

        X = self.ae.decode(X)

        return X, Y


    def remake_env_vectors(
        self,
        X: tp.List[tf.Tensor]
        ) -> None:

        Y = tf.identity(X)

        Y = self.ae.encode(Y)

        self.env_vectors = Y


    def create_memory(self) -> None:
        self.memory = Memory(
            self.env.observation_space,
            self.env.action_space,
            self.memory_topology,
            self.memory_activations,
            self.agent_tf_dtype)
        
        X = self.ae_code
        Y = self.memory_topology

        self.selectors = [
            selector(X, y, self.agent_tf_dtype) for y in Y]


    def train_memory(self, scene: int = 0, initial_iteration: int = 0) -> None:
        X = tf.identity(self.modelo.obs_set)
        Y = tf.nn.softmax(self.modelo.forward(X), 1)

        if self.agent_memory and initial_iteration == 0:
            self.memory.add_model()

        for run in range(initial_iteration, self.memory_iterations):
            tsi = time.time()

            if run > 0:
                self.memory.add_algorithm(self.memory_add_rate)
                shape = [w.shape[1] for w in self.memory.weights[::2]]
                score = self.env_recall()
                self.to_safe(scene, 'memory', 'add', run, shape, 0, 0, 0, score)

            cnt = 0
            last_aux = 0

            for i in range(self.memory_trainepochs):
                ts = time.time()
                self.memory.fit(X, Y,
                    self.memory_epochs,
                    self.memory_batch_size)
                ts = time.time() - ts

                aux = self.memory.behavior_error(Y, self.memory.predict(X))
                shape = [w.shape[1] for w in self.memory.weights[::2]]
                score = self.env_recall()
                self.to_safe(scene, 'memory', 'train', run, shape, aux, ts, 0, score)

                if aux < self.memory_train_accuracy:
                    cnt += 1

                if cnt == self.memory_breakpoint:
                    break

            self.memory.remove_algorithm(X, self.memory_remove_threshold)
            shape = [w.shape[1] for w in self.memory.weights[::2]]
            aux = self.memory.behavior_error(Y, self.memory.predict(X))
            score = self.env_recall()
            self.to_safe(scene, 'memory', 'remove', run, shape, aux, 0, 0, score)

            self.memory.combine_algorithm(
                self.memory_combine_threshold,
                X, Y,
                self.memory_epochs,
                self.memory_batch_size)
            shape = [w.shape[1] for w in self.memory.weights[::2]]
            aux = self.memory.behavior_error(Y, self.memory.predict(X))
            score = self.env_recall()
            self.to_safe(scene, 'memory', 'combine', run, shape, aux, 0, 0, score)

            # self.memory.fit(X, Y,
            #     self.memory_epochs,
            #     self.memory_batch_size)

            tsi = time.time() - tsi
            aux = self.memory.behavior_error(Y, self.memory.predict(X))
            self.to_safe(scene, 'memory', 'finished', run, shape, aux, 0)
            self.save_me(self.agent_code)

        self.memory.consolidate_model()


    def prepare_selector_training(self) -> None:
        self.selector_olders = [s.predict_selector(self.env_vectors,
            self.selector_threshold) for s in self.selectors]


    def train_selector(self, scene: int) -> None:
        new_masks = self.memory.predict_masks[:-1]

        X = self.env_vectors
        if self.agent_memory:
            Y = self.add_new_masks(new_masks)
        else:
            Y = new_masks

        self.selectors = [
            selector(X.shape[1], y.shape[1], self.agent_tf_dtype) for y in Y]

        ts = time.time()
        err = 0
        for s, y in zip(self.selectors, Y):
            for i in range(self.selector_tries):
                s.fit(X, y, self.selector_epochs)
                yp = s.predict(X)
                error = tf.reduce_mean(tf.keras.losses.mse(y, yp))
                aprint(f'{np.round(i / self.selector_tries, 3)} ~ {error}', False)
                if error < self.selector_error:
                    break
            err += error
        ts = time.time() - ts
        self.to_safe(scene, 'selector', 'train', 0, [0], err, ts)


    def add_new_masks(self, new_masks) -> tp.List[tf.Tensor]:
        ret = list()

        for n, o in zip(new_masks, self.selector_olders):
            z = tf.zeros(
                (o.shape[0], n.shape[1] - o.shape[1]),
                dtype=self.agent_tf_dtype)

            m = tf.concat([o, z], 1)
            ret += [tf.concat([n, m], 0)]
        return ret


    def training_cycle(self, environment: any) -> None:
        self.set_env(environment)
        self.reset()

        if self.agent_memory:
            y = tf.constant(self.env_description(), self.agent_tf_dtype)
            for dbn in self.dbn:
                y = dbn.forward(y)

            masks = [s.predict_selector(
                y, self.selector_threshold) for s in self.selectors]
            masks += [tf.ones_like(self.modelo.weights[-1])]

            self.memory.predict_masks = masks
            self.prepare_selector_training()

        self.train_arcnet()
        aprint(f'arcnet - {self.modelo.weights[2].shape}')
        self.train_dbn()
        aprint(f'dbn - {self.dbn[1].weights.shape}')
        self.train_memory()
        aprint(f'memory - {self.memory.weights[2].shape}')
        self.train_selector()
        self.agent_memory = True
        aprint(f'Done')


    def continue_vanilla_experiment(self) -> None:
        if len(self.safe) > 0:
            scn, model, _, iter, _, _, _, misc, _ = self.safe[-1].split(':')

            scn = int(scn)
            iter = int(iter)
            misc = int(misc)

            self.to_safe(scn, 'loaded', model, iter, [0], 0, 0)

            if iter == -1:
                scn += 1

            iter += 1
        else:
            scn = 0
            iter = 0
            misc = 0

        for env in range(scn, 5):
            ipd.clear_output()
            self.set_env(Environment(env + 1))
            arcnet = (iter == 0)
            self.reset(arcnet)

            if self.arcnet_current_run is None:
                self.arcnet_current_run = self.arcnet_runs[env]

            ts = time.time()
            self.env.endings = misc
            self.train_arcnet(scene = env, initial_iteration = iter)
            ts = time.time() - ts

            self.arcnet_current_run = None

            misc = 0

            model = 'arcnet'
            shape = [w.shape[1] for w in self.modelo.weights[::2]]
            self.to_safe(env, model, 'save', -1, shape, 0, ts)
            self.save_me(f'{self.agent_code}_{env}')
            iter = 0


    def continue_experiment(self) -> None:
        if len(self.safe) > 0:
            scn, model, _, iter, _, _, _, misc, _ = self.safe[-1].split(':')

            scn = int(scn)
            iter = int(iter)
            misc = int(misc)

            self.to_safe(scn, 'loaded', model, iter, [0], 0, 0)

            if (model == 'memory' or self.vanilla) and iter == -1:
                scn += 1
            elif iter != -1:
                model = {
                    'memory': 'ae',
                    'ae': 'arcnet',
                    'arcnet': 'memory'}[model]

            iter += 1
        else:
            scn = 0
            model = 'memory'
            iter = 0
            misc = 0

        for env in range(scn, 5):
            ipd.clear_output()
            if model == 'memory':
                self.set_env(Environment(env + 1))
                arcnet = (iter == 0)
                self.reset(arcnet)

                if self.agent_memory:
                    y = tf.constant(
                        self.env_description(),
                        self.agent_tf_dtype)

                    y = self.ae.encode(y)

                    masks = [s.predict_selector(
                        y, self.selector_threshold) for s in self.selectors]
                    masks += [tf.ones_like(self.memory.weights[-1])]

                    self.memory.predict_masks = masks
                    self.prepare_selector_training()

                if self.arcnet_current_run is None:
                    self.arcnet_current_run = self.arcnet_runs[env]

                ts = time.time()
                # self.train_arcnet(scene = env, initial_iteration = iter)
                self.env.endings = misc
                self.train_arcnet(scene = env, initial_iteration = iter)
                ts = time.time() - ts

                self.arcnet_current_run = None

                misc = 0

                model = 'arcnet'
                shape = [w.shape[1] for w in self.modelo.weights[::2]]
                self.to_safe(env, model, 'save', -1, shape, 0, ts)
                self.save_me(self.agent_code)
                iter = 0

            if model == 'arcnet':
                ts = time.time()
                self.train_autoencoder(scene = env, initial_iteration = iter)
                ts = time.time() - ts

                model = 'ae'
                shape = [w.shape[1] for w in self.ae.weights[::2]]

                self.to_safe(env, model, 'save', -1, shape, 0, ts)
                self.save_me(self.agent_code)
                iter = 0

            if model == 'ae':
                ts = time.time()
                self.train_memory(scene = env, initial_iteration = iter)
                self.train_selector(scene = env)
                ts = time.time() - ts

                self.agent_memory = True
                model = 'memory'

                shape = [w.shape[1] for w in self.memory.weights[::2]]
                self.to_safe(env, model, 'save', -1, shape, 0, ts)
                self.save_me(self.agent_code)
                self.save_me(f'{self.agent_code}_{env}')
                iter = 0


    def to_safe(
        self,
        scene: int,
        model: str,
        alg: str,
        iter: int,
        shape: tp.List[int],
        loss: float,
        tsi: float,
        misc: any = 0,
        score: any = 0
        ) -> None:

        ts = time.strftime("%d %H:%M:%S ")
        aux = f'{scene}:{model}:{alg}:{iter}:{shape}:{loss}:{tsi}:{misc}:{score}'
        aprint(ts + aux)
        self.safe += [aux]


    def save_me(self, code: str) -> None:
        aux_opt = None
        if self.modelo.opt is not None:
            aux_opt = self.modelo.opt.get_weights()

        save_object = {
            'agent': {
                'attributes': {
                    'agent_tf_dtype': self.agent_tf_dtype,
                    'arcnet_topology': self.arcnet_topology,
                    'arcnet_activations': self.arcnet_activations,
                    'arcnet_gamma': self.arcnet_gamma,
                    'arcnet_iterations': self.arcnet_iterations,
                    'arcnet_batchsize': self.arcnet_batchsize,
                    'arcnet_addrate': self.arcnet_addrate,
                    'arcnet_epochs': self.arcnet_epochs,
                    'arcnet_trainepochs': self.arcnet_trainepochs,
                    'arcnet_episodes': self.arcnet_episodes,
                    'arcnet_breakpoint': self.arcnet_breakpoint,
                    'arcnet_removethreshold': self.arcnet_removethreshold,
                    'arcnet_combinelimit': self.arcnet_combinelimit,
                    'arcnet_verbose': self.arcnet_verbose,
                    'arcnet_datasetsize': self.arcnet_datasetsize,
                    'arcnet_runs': self.arcnet_runs,
                    'ae_topology': self.ae_topology,
                    'ae_code': self.ae_code,
                    'ae_activations': self.ae_activations,
                    'ae_iterations': self.ae_iterations,
                    'ae_addrate': self.ae_addrate,
                    'ae_trainepochs': self.ae_trainepochs,
                    'ae_trainthreshold': self.ae_trainthreshold,
                    'ae_epochs': self.ae_epochs,
                    'ae_removethreshold': self.ae_removethreshold,
                    'ae_combinethreshold': self.ae_combinethreshold,
                    'memory_topology': self.memory_topology,
                    'memory_activations': self.memory_activations,
                    'memory_iterations': self.memory_iterations,
                    'memory_add_rate': self.memory_add_rate,
                    'memory_epochs': self.memory_epochs,
                    'memory_trainepochs': self.memory_trainepochs,
                    'memory_breakpoint': self.memory_breakpoint,
                    'memory_batch_size': self.memory_batch_size,
                    'memory_train_accuracy':self.memory_train_accuracy,
                    'memory_remove_threshold': self.memory_remove_threshold,
                    'memory_combine_threshold': self.memory_combine_threshold,
                    'selector_threshold': self.selector_threshold,
                    'selector_epochs': self.selector_epochs,
                    'selector_error': self.selector_error,
                    'selector_tries': self.selector_tries},

                'set': {
                    'selector_olders': self.selector_olders,
                    'agent_memory': self.agent_memory,
                    'agent_decisionvector': self.agent_decisionvector,
                    'safe': self.safe,
                    'env_vectors': self.env_vectors,
                    'safe_weights': self.safe_weights,
                    'arcnet_current_run': self.arcnet_current_run}},

            'arcnet': {
                'attributes': {
                    'weights': self.modelo.weights,
                    'obs_set': self.modelo.obs_set,
                    'nxt_set': self.modelo.nxt_set,
                    'act_set': self.modelo.act_set,
                    'rwd_set': self.modelo.rwd_set,
                    'end_set': self.modelo.end_set},

                'rebuild': {
                    'opt': aux_opt}},

            'ae': {
                'attributes': {
                    'weights': self.ae.weights}},

            'memory': {
                'attributes': {
                    'weights': self.memory.weights,
                    'train_masks': self.memory.train_masks,
                    'predict_masks': self.memory.predict_masks}},

            'selector': [{
                'attributes': {
                    'weights': selector.weights,
                    'bias': selector.bias}}
                for selector in self.selectors]}

        if self.vanilla:
            name = f'vanilla{code}'
        else:
            name = f'agent{code}'

        with open(f'{global_folder}/{name}.pickle', 'wb') as handle:
            pickle.dump(save_object, handle, protocol=pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def load_me(code: str, env: tp.Any, vanilla: bool = False) -> tp.Any:
        if vanilla:
            name = f'vanilla{code}'
        else:
            name = f'agent{code}'

        with open(f'{global_folder}/{name}.pickle', 'rb') as handle:
            prev = pickle.load(handle)

        prev['agent']['attributes']['agent_code'] = code
        prev['agent']['attributes']['environment_class'] = env

        new = agent(vanilla, **prev['agent']['attributes'])
        for k, v in prev['agent']['set'].items():
            setattr(new, k, v)

        for k, v in prev['arcnet']['attributes'].items():
            setattr(new.modelo, k, v)

        new.modelo.opt = agent.rebuild_opt(
            prev['arcnet']['attributes']['weights'],
            prev['arcnet']['rebuild']['opt'])

        for k, v in prev['ae']['attributes'].items():
            setattr(new.ae, k, v)

        for k, v in prev['memory']['attributes'].items():
            setattr(new.memory, k, v)

        for selector, vals in zip(new.selectors, prev['selector']):
            for k, v in vals['attributes'].items():
                setattr(selector, k, v)

            aux = [
                vals['attributes']['weights'],
                vals['attributes']['bias']]

        # env = int(new.safe[-1].split(':')[0])
        # new.set_env(Environment(env))

        return new

    @staticmethod
    def rebuild_opt(weights: tp.List[tf.Tensor], vars: tp.List[np.ndarray]):
        if vars is None:
            return None

        opt = tf.optimizers.RMSprop()

        # with tf.name_scope(opt._name):
        #     with tf.init_scope():
        #         opt._create_all_weights(weights)
        
        # opt.set_weights(vars)

        return opt
